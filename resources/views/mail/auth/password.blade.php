<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>New Password</title>
</head>
<body>
    <h1>Your New Password</h1>
    <p>Your new password is: <strong>{{ $password }}</strong></p>
    <p>Please change this password after logging in for security purposes.</p>
</body>
</html>
