@component('mail::message')
# Email Verification Code

Hello {{ $user->name }},

Your verification code is: **{{ $code }}**

Please use this code to verify your email address.

If you didn't request this code, you can safely ignore this email.

Regards,<br>
Your Application Team
@endcomponent
