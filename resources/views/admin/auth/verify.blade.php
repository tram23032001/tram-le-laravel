@extends('admin.layouts.page')
@section('title', 'Verify Code')
@section('content')
<!-- Verification Code Form -->
<section>
    <div class="container-alt">
        <div class="row">
            <div class="col-sm-12">
                <div class="wrapper-page">
                    <div class="m-t-40 account-pages">
                        <div class="text-center account-logo-box">
                            <h2 class="text-uppercase">
                                <a href="index.html" class="text-success">
                                    <span><img src="../admin/images/logo.png" alt="" height="36"></span>
                                </a>
                            </h2>
                        </div>
                        <div class="account-content">
                            <!-- Verification Code Form -->
                            <form class="form-horizontal" action="{{ route('verification.verify') }}" method="POST">
                                @csrf
                                <div class="form-group">
                                   <div class="col-xs-12">
                                    <label for="email">We send code to Email: {{ $email }}</label>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <div class="col-xs-12">
                                        <label for="verification_code">Enter Verification Code:</label>
                                        <input class="form-control" type="text" id="verification_code" name="verification_code" placeholder="Verification Code" required>
                                    </div>
                                </div>
                                <div class="form-group account-btn text-center m-t-10">
                                    <div class="col-xs-12">
                                        <button class="btn w-md btn-primary waves-effect waves-light" type="submit">Verify Code</button>
                                    </div>
                                </div>
                            </form>
                            <!-- End Verification Code Form -->
                            <div class="clearfix"></div>
                        </div>
                    </div>
                    <!-- End Account Pages -->
                </div>
                <!-- End Wrapper -->
            </div>
        </div>
    </div>
</section>
<!-- END HOME -->
@endsection
