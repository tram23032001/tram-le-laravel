@extends('admin.layouts.page')
@section('title', 'Register')
@section('content')
<!-- Registration Form -->
<section>
    <div class="container-alt">
        <div class="row">
            <div class="col-sm-12">
                <div class="wrapper-page">
                    <div class="m-t-40 account-pages">
                        <div class="text-center account-logo-box">
                            <h2 class="text-uppercase">
                                <a href="index.html" class="text-success">
                                    <span><img src="../admin/images/logo.png" alt="" height="36"></span>
                                </a>
                            </h2>
                        </div>
                        <div class="account-content">
                            <!-- Registration Form -->
                            <form class="form-horizontal" action="{{ route('admin.register') }}" method="POST">
                                @csrf
                                <div class="form-group">
                                    <div class="col-xs-12">
                                        <input class="form-control" type="text" name="name" placeholder="Username" required>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <div class="col-xs-12">
                                        <input class="form-control" type="email" name="email" placeholder="Email" required>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <div class="col-xs-12">
                                      <input class="form-control" type="password" name="password" placeholder="Password" required autocomplete="current-password">
                                    </div>
                                </div>
                                <div class="form-group">
                                    <div class="col-xs-12">
                                        <div class="checkbox checkbox-success">
                                            <input id="checkbox-signup" type="checkbox" checked="checked" required>
                                            <label for="checkbox-signup">I accept <a href="#">Terms and Conditions</a></label>
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group account-btn text-center m-t-10">
                                    <div class="col-xs-12">
                                        <button class="btn w-md btn-danger btn-bordered waves-effect waves-light" type="submit">SIGN UP FREE</button>
                                    </div>
                                </div>
                            </form>
                            <!-- End Registration Form -->
                            <div class="clearfix"></div>
                        </div>
                    </div>
                    <!-- End Account Pages -->
                    <div class="row m-t-50">
                        <div class="col-sm-12 text-center">
                            <p class="text-muted">Already have an account? <a href="{{ route('admin.login') }}" class="text-primary m-l-5"><b>Sign In</b></a></p>
                        </div>
                    </div>
                </div>
                <!-- End Wrapper -->
            </div>
        </div>
    </div>
</section>
<!-- END HOME -->
@endsection
