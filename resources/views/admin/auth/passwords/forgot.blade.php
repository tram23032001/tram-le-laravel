@extends('admin.layouts.page')
@section('title', 'Forgot Password')
@section('content')
<!-- Forgot Password Form -->
<section>
    <div class="container-alt">
        <div class="row">
            <div class="col-sm-12">
                <div class="wrapper-page">
                    <div class="m-t-40 account-pages">
                        <div class="text-center account-logo-box">
                            <h2 class="text-uppercase">
                                <a href="{{ url('/') }}" class="text-success">
                                    <span><img src="{{ asset('admin/images/logo.png') }}" alt="" height="36"></span>
                                </a>
                            </h2>
                        </div>
                        <div class="account-content">
                          <p class="login-box-msg">You forgot your password? Here you can easily retrieve a new password.</p>
                            <form class="form-horizontal" action="{{ route('password.email') }}" method="POST">
                                @csrf
                                <div class="form-group">
                                    <div class="col-xs-12">
                                        <label for="email">Email Address</label>
                                        <input id="email" type="email" class="form-control @error('email') is-invalid @enderror" name="email" value="{{ old('email') }}" required autocomplete="email" autofocus>
                                         @error('email')
                                            <span class="invalid-feedback" role="alert">
                                                <strong>{{ $message }}</strong>
                                             </span>
                                        @enderror
                                    </div>
                                </div>
                                <div class="form-group account-btn text-center m-t-10">
                                    <div class="col-xs-12">
                                        <button class="btn btn-primary waves-effect waves-light" type="submit">Send Password Reset Link</button>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div> <!-- End wrapper -->
            </div> <!-- end col -->
        </div> <!-- end row -->
    </div> <!-- end container -->
</section>
<!-- END Forgot Password Form -->
@endsection
