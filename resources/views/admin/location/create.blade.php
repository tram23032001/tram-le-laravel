<!-- new_location_popup.blade.php -->

<div id="myModal" class="modal">
  <div class="modal-content">
    <span class="close">&times;</span>
    <h2>New Location</h2>
    
    <!-- Form to create a new location -->
    <form method="POST" action="{{ route('locations.store') }}">
        @csrf

        <!-- Location Name -->
        <div class="form-group">
            <label for="name">Location Name:</label>
            <input type="text" id="name" name="name" class="form-control" placeholder="Enter location name">
        </div>

     <!-- Notes -->
        <div class="form-group">
            <label for="notes">Notes:</label>
            <textarea id="notes" name="notes" class="form-control" placeholder="Enter notes"></textarea>
        </div>
        <!-- Department -->
        <div class="form-group">
            <label for="department">Department:</label>
            <select id="department" name="department_id" class="form-control">
                @foreach($departments as $department)
                    <option value="{{ $department->id }}">{{ $department->name }}</option>
                @endforeach
            </select>
        </div>

        <!-- Các trường thông tin của bộ phận -->
        <div class="department-fields" style="display: none;">
            <div class="form-group">
                <label for="floor">Floor:</label>
                <input type="text" id="floor" name="floor" class="form-control" readonly>
            </div>

            <div class="form-group">
                <label for="unit">Unit:</label>
                <input type="text" id="unit" name="unit" class="form-control" readonly>
            </div>

            <!-- Thêm các trường thông tin khác của bộ phận -->
            <div class="form-group">
                <label for="building">Building:</label>
                <input type="text" id="building" name="building" class="form-control" readonly>
            </div>
            
             <div class="form-group">
                <div id="address"></div>
            </div>
            
            <div class="form-group">
                <label for="zip_code">Zip Code:</label>
                <input type="text" id="zip_code" name="zip_code" class="form-control" readonly>
            </div>
        </div>

        <!-- Save button -->
        <button type="submit" class="btn btn-primary">Save</button>
    </form>

    <script>
        // Lắng nghe sự kiện thay đổi của dropdown
        document.getElementById('department').addEventListener('change', function() {
            // Lấy giá trị đã chọn từ dropdown
            var departmentId = this.value;

            // Gửi yêu cầu AJAX để lấy thông tin của bộ phận
            $.ajax({
                url: '/departments/' + departmentId, // Thay đổi đường dẫn phù hợp với route của bạn
                type: 'GET',
                success: function(response) {
                    // Hiển thị các trường thông tin của bộ phận
                    var departmentFields = document.querySelector('.department-fields');
                    departmentFields.style.display = 'block';

                    // Điền thông tin từ response vào các trường tương ứng
                    document.getElementById('floor').value = response.floor;
                    document.getElementById('unit').value = response.unit;
                    document.getElementById('building').value = response.building;
                    document.getElementById('zip_code').value = response.zip_code;
                    // Hiển thị thông tin địa chỉ trong các thẻ div
                 // Hiển thị thông tin địa chỉ trong các thẻ div
                    var addressDiv = document.getElementById('address');
                    addressDiv.innerHTML = '<div class="form-group"><label for="city">City:</label><input type="text" id="city" name="city" class="form-control" value="' + response.address.city + '" readonly></div>' +
                                        '<div class="form-group"><label for="state">State:</label><input type="text" id="state" name="state" class="form-control" value="' + response.address.state + '" readonly></div>' +
                                        '<div class="form-group"><label for="street">Street:</label><input type="text" id="street" name="street" class="form-control" value="' + response.address.street + '" readonly></div>' +
                                        '<div class="form-group"><label for="country">Country:</label><input type="text" id="country" name="country" class="form-control" value="' + response.address.country + '" readonly></div>';
                },
                error: function(xhr) {
                    // Xử lý lỗi nếu có
                    console.error(xhr.responseText);
                }
            });
        });
    </script>
  </div>
</div>
