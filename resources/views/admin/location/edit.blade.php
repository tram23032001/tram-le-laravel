<!-- admin.location.edit.blade.php -->

<div id="editModal{{ $location->id }}" class="modal">
    <div class="modal-content">
        <span class="close">&times;</span>
        <h2>Edit Location</h2>

        <!-- Form to edit a location -->
        <form method="POST" action="{{ route('locations.update', $location->id) }}">
            @csrf
            @method('PUT')

            <!-- Location Name -->
            <div class="form-group">
                <label for="name">Location Name:</label>
                <input type="text" id="name" name="name" class="form-control" value="{{ $location->name }}" placeholder="Enter location name">
            </div>

            <!-- Notes -->
            <div class="form-group">
                <label for="notes">Notes:</label>
                <textarea id="notes" name="notes" class="form-control" placeholder="Enter notes">{{ $location->notes }}</textarea>
            </div>

            <!-- Department -->
            <div class="form-group">
                <label for="department">Department:</label>
                <select id="department" name="department_id" class="form-control">
                    @foreach($departments as $department)
                        <option value="{{ $department->id }}" {{ $location->department_id == $department->id ? 'selected' : '' }}>{{ $department->name }}</option>
                    @endforeach
                </select>
            </div>

            <!-- Additional department fields -->
            <div class="department-fields" style="display: none;">
                <!-- Floor -->
                <div class="form-group">
                    <label for="floor">Floor:</label>
                    <input type="text" id="floor" name="floor" class="form-control" value="{{ $location->floor }}" readonly>
                </div>

                <!-- Unit -->
                <div class="form-group">
                    <label for="unit">Unit:</label>
                    <input type="text" id="unit" name="unit" class="form-control" value="{{ $location->unit }}" readonly>
                </div>

                <!-- Building -->
                <div class="form-group">
                    <label for="building">Building:</label>
                    <input type="text" id="building" name="building" class="form-control" value="{{ $location->building }}" readonly>
                </div>

                <!-- Address -->
             <div class="form-group">
                <div id="address"></div>
            </div>

                <!-- Zip Code -->
                <div class="form-group">
                    <label for="zip_code">Zip Code:</label>
                    <input type="text" id="zip_code" name="zip_code" class="form-control" value="{{ $location->zip_code }}" readonly>
                </div>
            </div>

            <!-- Save button -->
            <button type="submit" class="btn btn-primary">Save Changes</button>
        </form>

        <!-- Script to display department information when selected -->
        <script>
            document.getElementById('department').addEventListener('change', function() {
                var departmentId = this.value;

                $.ajax({
                    url: '/departments/' + departmentId,
                    type: 'GET',
                    success: function(response) {
                        var departmentFields = document.querySelector('.department-fields');
                        departmentFields.style.display = 'block';

                        // Hiển thị thông tin địa chỉ trong các thẻ div
                        var addressDiv = document.getElementById('address');
                        addressDiv.innerHTML = '<div class="form-group"><label for="city">City:</label><input type="text" id="city" name="city" class="form-control" value="' + response.address.city + '" readonly></div>' +
                                              '<div class="form-group"><label for="state">State:</label><input type="text" id="state" name="state" class="form-control" value="' + response.address.state + '" readonly></div>' +
                                              '<div class="form-group"><label for="street">Street:</label><input type="text" id="street" name="street" class="form-control" value="' + response.address.street + '" readonly></div>' +
                                              '<div class="form-group"><label for="country">Country:</label><input type="text" id="country" name="country" class="form-control" value="' + response.address.country + '" readonly></div>';

                        document.getElementById('zip_code').value = response.zip_code;
                    },
                    error: function(xhr) {
                        console.error(xhr.responseText);
                    }
                });
            });
        </script>
    </div>
</div>
