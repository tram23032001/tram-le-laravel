
<!-- Form đăng nhập -->
@extends('admin.layouts.admin')
@section('title', '')
@section('content')
<section>
                <!-- Start content -->
                <div class="content">
                    <div class="container">
                        <div class="row">
							<div class="col-xs-12">
								<div class="page-title-box">
                                    <h4 class="page-title">Locations </h4>
                                    <ol class="breadcrumb p-0 m-0">
                                        <li>
                                            <a href="#">Zircos</a>
                                        </li>
                                        <li>
                                            <a href="#">Tables </a>
                                        </li>
                                        <li class="active">
                                            Datatable
                                        </li>
                                    </ol>
                                    <div class="clearfix"></div>
                                </div>
							</div>
						</div>
                        <!-- end row -->
                        <div class="row">
                            <div class="col-sm-12">
                                <div class="card-box table-responsive">
                                    <div class="row">
                                        <div class="col-sm-6">
                                            <div id="datatable_filter" class="dataTables_filter">
                                                <label>Search:<input type="search" class="form-control input-sm" placeholder="" aria-controls="datatable"></label>
                                            </div>
                                        </div>
                                        <div class="col-sm-6 text-right">
                                            <button class="btn btn-primary btn-sm" onclick="openPopup()">New Locations</button>
                                            <!-- Include file popup blade -->
                                                @include('admin.location.create')
                                            <button class="btn btn-info btn-sm" onclick="openImportPopup()">Import Locations</button>
                                              @include('admin.location.import')
                                        </div>
                                    </div>
                                     <table id="datatable-buttons" class="table table-striped table-colored table-info">
                                        <thead>
                                            <tr>
                                                <th style="width: 20%;">ID</th>
                                                <th style="width: 20%;">Name</th>
                                                <th style="width: 20%;">Address</th>
                                                <th style="width: 20%;">Notes</th>
                                                <th style="width: 20%;">Actions</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            @foreach($locations as $location)
                                            <tr>
                                                <td>{{ $location->id }}</td>
                                                <td>{{ $location->name }}</td>
                                                <td>{{ $location->department->address['city'] }}, {{  $location->department->address['state'] }}, {{  $location->department->address['country'] }},{{ $location->department->zip_code }}</td>
                                                <td>{{ $location->notes }}</td>
                                                <td>
                                               <button class="btn btn-primary btn-sm" onclick="openEditPopup('{{ $location->id }}', 'location')">Edit</button>
                                                    @include('admin.location.edit', ['location' => $location])
                                                    <button class="btn btn-info btn-sm">Copy</button>
                                             <form id="deleteForm" action="{{ route('locations.destroy', $location->id) }}" method="POST" style="display: none;">
                                                @csrf
                                                @method('DELETE')
                                            </form>

                                            <button class="btn btn-danger btn-sm" onclick="deleteItemConfirmation('{{ $location->id }}', 'location')">Delete</button>
                                                </td>
                                            </tr>
                                            @endforeach
                                        </tbody>
                                    </table>
                                        @if($locations->hasPages())
                                            <div class="pagination float-right margin-20">
                                                {{ $locations->appends($query = '')->links() }}
                                            </div>
                                        @endif        
                                </div>
                            </div>
                        </div>

                    </div> <!-- container -->

                </div> <!-- content -->

</section>
<!-- END HOME -->

@endsection
