<!DOCTYPE html>
<html lang="en">
<head>
    
    <!-- App favicon -->
    <link rel="shortcut icon" href="{{ asset('../admin/images/favicon.ico') }}">
    <!-- App title -->
    <title>Zircos - Responsive Admin Dashboard Template</title>

    <!-- App css -->
     <meta name="csrf-token" content="{{ csrf_token() }}">
    <link href="{{ asset('../admin/css/bootstrap.min.css') }}" rel="stylesheet" type="text/css" />
    <link href="{{ asset('../admin/css/core.css') }}" rel="stylesheet" type="text/css" />
    <link href="{{ asset('../admin/css/components.css') }}" rel="stylesheet" type="text/css" />
    <link href="{{ asset('../admin/css/icons.css') }}" rel="stylesheet" type="text/css" />
    <link href="{{ asset('../admin/css/pages.css') }}" rel="stylesheet" type="text/css" />
    <link href="{{ asset('../admin/css/menu.css') }}" rel="stylesheet" type="text/css" />
    <link href="{{ asset('../admin/css/responsive.css') }}" rel="stylesheet" type="text/css" />
    
    <!-- Additional CSS -->
    <link rel="stylesheet" type="text/css" href="{{ asset('../admin/css/icons.css') }}">
    <script src="{{ asset('../admin/js/modernizr.min.js') }}"></script>
    <style>
    .m-t-40{
        margin-top: 150px !important;
    }
    </style>
</head>
 <body class="bg-transparent">
  @include('admin.common.navbar')
  @yield('content')
   <!-- Sử dụng Blade Component Navigation -->
    @include('admin.common.footer')
  <script>
    var resizefunc = [];
</script>
    <script src="{{ asset('../admin/js/jquery.min.js') }}"></script>
    <script src="{{ asset('../admin/js/bootstrap.min.js') }}"></script>
    <script src="{{ asset('../admin/js/detect.js') }}"></script>
    <script src="{{ asset('../admin/js/fastclick.js') }}"></script>
    <script src="{{ asset('../admin/js/jquery.blockUI.js') }}"></script>
    <script src="{{ asset('../admin/js/waves.js') }}"></script>
    <script src="{{ asset('../admin/js/jquery.slimscroll.js') }}"></script>
    <script src="{{ asset('../admin/js/jquery.scrollTo.min.js') }}"></script>

    <!-- App js -->
    <script src="{{ asset('../admin/js/jquery.core.js') }}"></script>
    <script src="{{ asset('../admin/js/jquery.app.js') }}"></script>
</body>


</html>
