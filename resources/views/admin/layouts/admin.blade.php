<!DOCTYPE html>
<html lang="en">
<head>
    <title>@yield('title', 'WEB MANAGER')</title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="csrf-token" content="{{ csrf_token() }}">
    @include('admin.common.head')
    @stack('styles') <!-- Đưa vào stack các đoạn mã CSS -->
</head>

    <body class="fixed-left">
      <div id="wrapper">
   <!-- Sử dụng Blade Component Navigation -->
    @include('admin.common.navbar')
    @include('admin.common.sidebar')
       <div class="content-page">
    @yield('content')
    </div>
    @include('admin.common.footer')
    @include('admin.common.script')
    </div>
    @stack('scripts') <!-- Đưa vào stack các đoạn mã JavaScript -->
</body>


</html>
