@extends('admin.layouts.admin')
@section('title', 'Manage Roles')

@section('content')
<section>
<section class="content-header">
        <div class="container-fluid">
            <div class="row mb-2">
                <div class="col-sm-6">
                    <div class="app-title">
                        <ul class="app-breadcrumb breadcrumb">
                    <ol class="breadcrumb float-sm-left">
                        <li class="breadcrumb-item"><a href="{{ route('admin.home') }}"> <i class="nav-icon fas fa fa-home"></i> Trang chủ</a></li>
                        <li class="breadcrumb-item"><a href="{{ route('role.index') }}"Role </a></li>
                        <li class="breadcrumb-item active">Create</li>
                    </ol>
                   </ul>
                  </div>
                </div>
            </div>
        </div><!-- /.container-fluid -->
    </section>
        <section class="content">
        @include('admin.role.form')
    </section>
</section>

@endsection
