@extends('admin.layouts.admin')
@section('title', 'Manage Roles')

@section('content')
<section>
    <div class="content">
        <div class="container">
            <div class="row">
            <div class="col-xs-6">
                <!-- User Section -->
                <div class="page-title-box">
                    <h4 class="page-title">Users</h4>
                </div>
                <!-- User Table Goes Here -->
            </div>
            <div class="col-xs-6">
                <!-- Role Section -->
                <div class="page-title-box">
                    <a href="{{ route('role.index') }}"><h4 class="page-title">Roles</h4></a>
                </div>
                <!-- Role Table Goes Here -->
            </div>
        </div>
            
            <!-- Role Table -->
            <div class="row">
                <div class="col-sm-12">
                    <div class="card-box table-responsive">
                        <div class="row">
                            <div class="col-sm-6">
                                <div id="datatable_filter" class="dataTables_filter">
                                    <label>Search:<input type="search" class="form-control input-sm" placeholder="" aria-controls="datatable"></label>
                                </div>
                            </div>
                            <div class="col-sm-6 text-right">
                                <a href="{{ route('role.create') }}" class="btn btn-primary btn-sm">New Role</a>
                            </div>
                        </div>
                        
                        <!-- Role Table -->
                        <table id="datatable-buttons" class="table table-striped table-colored table-info">
                            <thead>
                                <tr>
                                    <th>ID</th>
                                    <th>Name</th>
                                    <th>Description</th>
                                    <th>Actions</th>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach($roles as $role)
                                <tr>
                                    <td>{{ $role->id }}</td>
                                    <td>{{ $role->name }}</td>
                                    <td>{{ $role->description }}</td>
                                    <td>
                                        <a href="{{ route('role.edit', $role->id) }}" class="btn btn-primary btn-sm">Edit</a>
                                        <button class="btn btn-info btn-sm">Copy</button>
                                        <form action="{{ route('role.destroy', $role->id) }}" method="POST" style="display: inline;">
                                            @csrf
                                            @method('DELETE')
                                            <button type="submit" class="btn btn-danger btn-sm" onclick="return confirm('Are you sure you want to delete this role?')">Delete</button>
                                        </form>
                                    </td>
                                </tr>
                                @endforeach
                            </tbody>
                        </table>
                        @if($roles->hasPages())
                                            <div class="pagination float-right margin-20">
                                                {{ $roles->appends($query = '')->links() }}
                                            </div>
                         @endif  
                    </div>
                </div>
            </div>
        </div> <!-- container -->
    </div> <!-- content -->
</section>
@endsection
