@extends('admin.layouts.admin')
@section('title', 'Manage Roles')

@section('content')
<section>
    <section class="content-header">
        <div class="container-fluid">
            <div class="row mb-2">
                <div class="col-sm-6">
                    <div class="app-title">
                        <ul class="app-breadcrumb breadcrumb">
                            <ol class="breadcrumb float-sm-left">
                                <li class="breadcrumb-item"><a href="{{ route('admin.home') }}"> <i class="nav-icon fas fa fa-home"></i> Trang chủ</a></li>
                                <li class="breadcrumb-item"><a href="{{ route('role.index') }}">Tour</a></li>
                                <li class="breadcrumb-item active">Chỉnh sửa</li>
                            </ol>
                        </ul>
                    </div>
                </div>
            </div>
        </div><!-- /.container-fluid -->
    </section>
    <section class="content">
        <div class="container-fluid">
            <form role="form" action="" method="POST" enctype="multipart/form-data">
                @csrf
                <div class="row">
                    <div class="col-md-12">
                        <div class="card card-primary">
                            <!-- form start -->
                            <div class="card-body">
                                <div class="form-group {{ $errors->first('name') ? 'has-error' : '' }} ">
                                    <label for="inputEmail3" class="control-label default">Name <sup class="text-danger">(*)</sup></label>
                                    <div>
                                        <input type="text" maxlength="100" class="form-control"  name="name" value="{{ old('name',isset($role) ? $role->name : '') }}">
                                        <span class="text-danger "><p class="mg-t-5">{{ $errors->first('name') }}</p></span>
                                    </div>
                                </div>
                                <div class="form-group {{ $errors->first('description') ? 'has-error' : '' }}">
                                    <label for="inputEmail3" class="control-label default">Description </label>
                                    <div>
                                        <textarea name="description" style="resize:vertical" class="form-control" >{{ old('description',isset($role) ? $role->description : '') }}</textarea>
                                        <span class="text-danger"><p class="mg-t-5">{{ $errors->first('description') }}</p></span>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label for="exampleInputEmail1">Permission setup</label>
                                    <div class="col-md-12 permission_role" style="padding: 0px;">
                                        <table class="table table-bordered">
                                            <thead>
                                                <tr>
                                                    <th>Module</th>
                                                    <th>None</th>
                                                    <th>Full</th>
                                                    <th>Select</th>
                                                    <th>View</th>
                                                    <th>Create</th>
                                                    <th>Edit</th>
                                                    <th>Delete</th>
                                                </tr>
                                            </thead>
                                            <tbody>
 @if($permissionGroups)
    @foreach($permissionGroups as $permissionGroup)
        <tr>
           <td>{{$permissionGroup->name}}</td>
           <td> <input type="radio" name="permission_options[{{$permissionGroup->name}}]" class="permission_option" value="none" data-group="{{ $permissionGroup->name }}"> None</td>
           <td> <input type="radio" name="permission_options[{{$permissionGroup->name}}]" class="permission_option" value="full" data-group="{{ $permissionGroup->name }}"> Full</td>
           <td> <input type="radio" name="permission_options[{{$permissionGroup->name}}]" class="permission_option" value="select" data-group="{{ $permissionGroup->name }}"> Select</td>

    @for($i = 1; $i <= 4; $i++)
        <td>
            @foreach($permissionGroup->permissions as $permissions)
                @if($permissions->type == $i)
                    
                    <div class="col-md-3 role-item">
                        <div class="icheck-primary d-inline">
                            <input type="checkbox" class="{{safeTitle($permissionGroup->name)}}"
                                {{ isset($listPermission) && in_array($permissions->id, $listPermission) ? 'checked' : '' }}
                                value="{{$permissions->id}}" name="permissions[]" id="checkbox{{ $permissions->id }}">
                            <label for="checkbox{{ $permissions->id }}">
                            </label>
                        </div>
                    </div>
                @endif
            @endforeach
        </td>
    @endfor
</tr>
@endforeach
@endif


                                            </tbody>
                                        </table>
                                    </div>
                                </div> 
                            </div>
                            <!-- /.card-body -->
                        </div>
                    </div>
                    <div class="col-md-12">
                        <div class="card">
                            <div class="card-body">
                                <div class="btn-set">
                                    <button type="submit" name="submit" class="btn btn-info">
                                        <i class="fa fa-save"></i> Save
                                    </button>
                                    <button type="reset" name="reset" value="reset" class="btn btn-danger">
                                        <i class="fa fa-undo"></i> Reset
                                    </button>
                                </div>
                            </div>
                            <!-- /.card-body -->
                        </div>
                    </div>
                </div>
            </form>
        </div>
    </section>
</section>

<script src="https://code.jquery.com/jquery-3.6.0.min.js"></script>
<script>
$(document).ready(function() {
    // Xử lý sự kiện change cho nút radio "None"
    $('.permission_option[value=none]').change(function() {
        var groupName = $(this).data('group');
        var $checkboxes = $('input[type=checkbox][data-group="' + groupName + '"]');
        $checkboxes.prop('checked', false).prop('disabled', true);
    });

    // Xử lý sự kiện change cho nút radio "Full"
   $('.full-checkbox').on('click', function() {
        var row = $(this).closest('tr');
        row.find('input[type=checkbox]').prop('checked', this.checked);
    });
    
    // Xử lý sự kiện change cho nút radio "Select"
    $('.permission_option[value=select]').change(function() {
        var groupName = $(this).data('group');
        var $checkboxes = $('input[type=checkbox][data-group="' + groupName + '"]');
        $checkboxes.prop('disabled', false);
    });
});

</script>

@endsection
