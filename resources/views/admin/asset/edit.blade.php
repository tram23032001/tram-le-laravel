<div id="editModal{{ $asset->id }}" class="modal">
    <div class="modal-content">
        <span class="close" onclick="closeModal('{{ $asset->id }}')">&times;</span>
        <h2>Edit Asset</h2>

        <!-- Form to edit an asset -->
        <form method="POST" action="{{ route('assets.update', $asset->id) }}">
            @csrf
            @method('PUT')

            <!-- Asset Name -->
            <div class="form-group">
                <label for="name">Asset Name:</label>
                <input type="text" id="name" name="name" class="form-control" value="{{ $asset->name }}" placeholder="Enter asset name">
            </div>

            <!-- Notes -->
            <div class="form-group">
                <label for="notes">Notes:</label>
                <textarea id="notes" name="notes" class="form-control" placeholder="Enter notes">{{ $asset->notes }}</textarea>
            </div>

            <!-- Condition -->
            <div class="form-group">
                <label for="condition">Condition:</label>
                <select id="condition" name="condition_id" class="form-control">
                    @foreach($conditions as $condition)
                        <option value="{{ $condition->id }}" {{ $asset->condition_id == $condition->id ? 'selected' : '' }}>{{ $condition->name }}</option>
                    @endforeach
                </select>
            </div>

            <!-- Category -->
            <div class="form-group">
                <label for="category">Category:</label>
                <select id="category" name="category_id" class="form-control">
                    @foreach($categories as $category)
                        <option value="{{ $category->id }}" {{ $asset->category_id == $category->id ? 'selected' : '' }}>{{ $category->name }}</option>
                    @endforeach
                </select>
            </div>

            <!-- Location -->
            <div class="form-group">
                <label for="location">Location:</label>
                <select id="location" name="location_id" class="form-control">
                    @foreach($locations as $location)
                        <option value="{{ $location->id }}" {{ $asset->location_id == $location->id ? 'selected' : '' }}>
                            {{ $location->name }} - {{ $location->department->name }} - {{ $location->department->address['city'] }}, {{ $location->department->address['state'] }}, {{ $location->department->address['country'] }}
                        </option>
                    @endforeach
                </select>
            </div>

            <!-- Additional asset fields -->

            <!-- Manufacturer -->
            <div class="form-group">
                <label for="manufacturer">Manufacturer:</label>
                <select id="manufacturer" name="manufacturer_id" class="form-control">
                    @foreach($manufacturers as $manufacturer)
                        <option value="{{ $manufacturer->id }}" {{ $asset->model->manufacturer_id == $manufacturer->id ? 'selected' : '' }}>{{ $manufacturer->name }}</option>
                    @endforeach
                </select>
            </div>

          <!-- Model -->
            <div class="form-group">
                <label for="model">Model:</label>
                <select id="model" name="model_id" class="form-control">
                    @foreach($models as $model)
                        @if($model->manufacturer_id == $asset->model->manufacturer_id)
                            <option value="{{ $model->id }}" {{ $asset->model_id == $model->id ? 'selected' : '' }}>{{ $model->name }}</option>
                        @endif
                    @endforeach
                </select>
            </div>

            <!-- Supplier -->
            <div class="form-group">
                <label for="supplier">Supplier:</label>
                <select id="supplier" name="supplier_id" class="form-control">
                    @foreach($suppliers as $supplier)
                        <option value="{{ $supplier->id }}" {{ $asset->supplier_id == $supplier->id ? 'selected' : '' }}>{{ $supplier->name }}</option>
                    @endforeach
                </select>
            </div>

            <!-- Serial -->
            <div class="form-group">
                <label for="serial">Serial:</label>
                <input type="text" id="serial" name="serial" class="form-control" placeholder="Enter serial" value="{{ $asset->serial }}">
            </div>

            <!-- Date -->
            <div class="form-group">
                <label for="date">Date:</label>
                <input type="date" id="date" name="date" class="form-control" value="{{ \Carbon\Carbon::parse($asset->date)->format('Y-m-d') }}">
            </div>

            <!-- Warranty -->
            <div class="form-group">
                <label for="warranty">Warranty(month):</label>
                <input type="text" id="warranty" name="warranty" class="form-control" placeholder="Enter warranty" value="{{ $asset->warranty }}">
            </div>

            <!-- Price -->
            <div class="form-group">
                <label for="price">Price:</label>
                <input type="text" id="price" name="price" class="form-control" placeholder="Enter price"
                value="{{ $asset->price }}">
            </div>

            <!-- Upload Images -->
            <div class="form-group">
                <label for="images">Upload Images:</label>
                <div id="image-container">
                    <!-- Display existing images -->
                        @if(!is_null($asset->images))
                            @foreach($asset->images as $image)
                            <div class="thumbnail">
                                <img src="{{ asset($image->path) }}" alt="Asset Image">
                              
                            </div>
                            @endforeach
                        @endif
                </div>
                <!-- Input for uploading new images -->
                <input type="file" id="images" name="images[]" class="form-control" multiple onchange="previewImages()">
            </div>

            <!-- Save button -->
            <button type="submit" class="btn btn-primary">Save Changes</button>
        </form>

       
    <script>

    </script>

    </div>
</div>
