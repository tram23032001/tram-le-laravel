@extends('admin.layouts.admin')
@section('title', 'Print Result')
@section('content')
<style>
@media (min-width: 768px) {
    .asset-box {
        --cols: {{ $cols }};
        --rows: {{ $rows }};
    }
}
.asset-box {
    /* Thiết lập kích thước dựa trên số lượng cột */
    width: calc((100% / var(--cols)) - 20px); /* Độ rộng phải tính cả margin */
    height: 200px; /* Đảm bảo mỗi ô có kích thước phù hợp */
    border: 1px solid #000; /* Đường viền ô */
    padding: 10px; /* Khoảng cách giữa nội dung và viền của ô */
    margin: 10px; /* Khoảng cách giữa các ô */
    float: left; /* Hiển thị ô theo hàng ngang */
}

.invoice-wrapper {
  display: flex;
  justify-content: space-between;
  padding: 0 1rem;
}
.qr-code1 {
  max-width: 100%; /* Hình ảnh sẽ không vượt quá chiều rộng của cha */
  height: auto; /* Đảm bảo tỷ lệ khung hình bảo toàn */
  width: 160px; /* Đặt kích thước cố định cho hình ảnh QR */
  height: 170px; /* Đặt kích thước cố định cho hình ảnh QR */
}
.invoice-company { text-align: right; }

.invoice-company-name {
  font-size: 0.9rem;
  margin-bottom: 1.25rem;
}

.invoice-company-address {
  display: flex;
  flex-direction: column;
  gap: 0.25rem;
}

.invoice-logo {
  width: 5rem;
  height: 5rem;
}

.invoice-billing-company {
  font-size: 0.65rem;
  margin-bottom: 0.25rem;
}

.invoice-billing-address {
  display: flex;
  flex-direction: column;
  gap: 0.25rem;
}

.invoice-info {
  display: flex;
  justify-content: space-between;
  gap: 2rem; 
  margin: 0.25rem 0;
}

.invoice-info:nth-of-type(5) { margin-top: 1.5rem; }
.invoice-info-value { font-weight: 900; }
.invoicetable { width: 100%; }
.invoice-table, th, td { border-collapse: collapse; }

th, td {
  width: calc((600px - 3rem) / 4);
  text-align: center;
  padding: 0.75rem;
}

tr:nth-of-type(1) { background-color: rgba(0,0,0,0.2); }
tr:nth-of-type(2), tr:nth-of-type(3) { border-bottom: 0.5px solid rgba(0,0,0,0.25); }

.invoice-total { font-weight: 900; }

.invoice-print {
  font-size: 1.25rem;
  margin: 0 auto;
  cursor: pointer;
  border: 1.25px solid black;
  border-radius: 50%;
  padding: 0.6rem;
}

.invoice-print:active {
  background-color: black;
  color: white;
}</style>

<section>
    <div class="content">
        <div class="container">
            <!-- Hiển thị nội dung in -->
            <h1>Print Result</h1> <ion-icon name="print-outline" class='invoice-print'></ion-icon>
           <div class="asset-container">
    @foreach($selectedAssets as $asset)
        @for ($m = 0; $m < $rows; $m++) <!-- Vòng lặp cho hàng -->
            <div class="asset-row"> <!-- Container cho mỗi hàng -->
                @for ($k = 0; $k < $cols; $k++) <!-- Vòng lặp cho cột -->
                    <div class="asset-box">
                        <div class="row">
                            <div class="col-md-6">
                                <!-- Hiển thị thông tin của asset -->
                                @foreach($selectedFields as $field)
                                    @if($field === 'location_department')
                                        <p>{{ $asset->location->name }} / {{ $asset->location->department->name }}</p>
                                    @elseif($field === 'model_serial')
                                        <p>{{ $asset->model->name }} / {{ $asset->serial }}</p>
                                      @elseif($field === 'qr_code')
                                          <!-- Nếu trường là 'qr_code', không hiển thị gì cả -->
                                      @else
                                        <p>{{ ucfirst($field) }}: {{ $asset->$field }}</p>
                                    @endif
                                @endforeach
                            </div>
                            <div class="col-md-6">
                                <img src="{{ asset($asset->qr_code) }}" alt="QR Code" class="img-fluid qr-code1">
                            </div>
                        </div>
                    </div>
                @endfor
            </div>
        @endfor
    @endforeach
</div>

           
        </div>
    </div>
</section>

<script type="module" src="https://unpkg.com/ionicons@7.1.0/dist/ionicons/ionicons.esm.js"></script>
<script nomodule src="https://unpkg.com/ionicons@7.1.0/dist/ionicons/ionicons.js"></script>
<script>
    const print = document.querySelector('.invoice-print');
    const media = window.matchMedia('print');

    const update = (e) => print.style.display = e.matches ? 'none' : 'block';

    function convert() {
        media.addEventListener('change',update,false);
        window.print();
    }

    print.addEventListener('click',convert,false);
</script>
@endsection
