@extends('admin.layouts.admin')
@section('title', 'Select Assets')
@section('content')
<section>
    <div class="content">
        <div class="container">
            <div class="row">
                <!-- Tiêu đề -->
                <div class="col-xs-6">
                    <div class="page-title-box">
                        <h4 class="page-title">Select Assets</h4>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-xs-12">
                    <div class="card-box table-responsive">
                        <!-- Nút Next -->
                        <div class="text-right mb-5">
                            <form id="assetForm" action="{{ route('print.template') }}" method="POST">
                                @csrf
                                <!-- Trường ẩn để lưu danh sách tài sản đã chọn -->
                                <input type="hidden" name="selectedAssets" id="selectedAssets">
                                <button type="submit" class="btn btn-primary">Next</button>
                            </form>
                        </div>
                        <!-- Bảng danh sách tài sản -->
                        <table id="datatable-buttons" class="table table-striped table-colored table-info">
                            <thead>
                                <tr>
                                    <th>ID</th>
                                    <th>Name</th>
                                    <th>Location</th>
                                    <th>Model</th>
                                    <th>Select</th>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach($assets as $asset)
                                <tr>
                                    <td>{{ $asset->id }}</td>
                                    <td>{{ $asset->name }}</td>
                                    <td>{{ $asset->location->name }}</td>
                                    <td>{{ $asset->model->name }}</td>
                                    <td>
                                        <!-- Checkbox để chọn tài sản -->
                                        <input type="checkbox" name="selected_assets[]" value="{{ $asset->id }}">
                                    </td>
                                </tr>
                                @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>

<script>
// Xử lý sự kiện khi nhấn nút Next
document.getElementById('assetForm').addEventListener('submit', function(event) {
    event.preventDefault();
    var selectedAssets = [];
    // Lặp qua các checkbox và thu thập ID của các tài sản đã chọn
    var checkboxes = document.getElementsByName('selected_assets[]');
    for (var i = 0; i < checkboxes.length; i++) {
        if (checkboxes[i].checked) {
            selectedAssets.push(checkboxes[i].value);
        }
    }
    // Xây dựng URL với tham số selectedAssets
    var url = "{{ route('print.template') }}?selectedAssets=" + selectedAssets.join(',');
    // Chuyển hướng đến URL mới
    window.location.href = url;
});

</script>

@endsection