@extends('admin.layouts.admin')
@section('title', 'Print Template')
@section('content')
<section>
    <div class="content">
        <div class="container">
            <div class="row">
                <!-- Tiêu đề -->
                <div class="col-xs-12">
                    <div class="page-title-box">
                        <h4 class="page-title">
                            <a href="{{ route('assets.select') }}">Select Asset</a>
                        </h4>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-md-6">
                    <!-- Form chọn trường hiển thị -->
                    <div class="card">
                        <div class="card-body">
                            <h4 class="card-title">Step 1: Select fields to be printed on label</h4>
                            <form id="printForm" method="POST" action="{{ route('print.assets') }}">
                                @csrf
                                <div class="form-group">
                                    <label>Select fields:</label><br>
                                    <div class="form-check">
                                        <input class="form-check-input" type="checkbox" id="checkName" name="selectedFields[]" value="name">
                                        <label class="form-check-label" for="checkName">Asset Name</label>
                                    </div>
                                    <div class="form-check">
                                        <input class="form-check-input" type="checkbox" id="checkCode" name="selectedFields[]" value="code">
                                        <label class="form-check-label" for="checkCode">Asset Code</label>
                                    </div>
                                    <div class="form-check">
                                        <input class="form-check-input" type="checkbox" id="checkDate" name="selectedFields[]" value="date">
                                        <label class="form-check-label" for="checkDate">Date Purchased</label>
                                    </div>
                                    <div class="form-check">
                                        <input class="form-check-input" type="checkbox" id="checkWarranty" name="selectedFields[]" value="warranty">
                                        <label class="form-check-label" for="checkWarranty">Warranty</label>
                                    </div>
                                    <div class="form-check">
                                        <input class="form-check-input" type="checkbox" id="checkSupplier" name="selectedFields[]" value="supplier_id">
                                        <label class="form-check-label" for="checkSupplier">Vendor</label>
                                    </div>
                                    <div class="form-check">
                                        <input class="form-check-input" type="checkbox" id="checkLocationDepartment" name="selectedFields[]" value="location_department">
                                        <label class="form-check-label" for="checkLocationDepartment">Location/Department</label>
                                    </div>
                                    <div class="form-check">
                                        <input class="form-check-input" type="checkbox" id="checkModelSerial" name="selectedFields[]" value="model_serial">
                                        <label class="form-check-label" for="checkModelSerial">Model/Serial</label>
                                    </div>
                                    <!-- Thêm các checkbox khác tương ứng với các thuộc tính bạn muốn hiển thị -->
                                </div>
                                <input type="hidden" id="qrCode" name="selectedFields[]" value="qr_code">
                                <input type="hidden" name="selectedAssets" value="{{ json_encode($selectedAssets) }}">

                                <!-- Nút Print -->
                                <div class="text-left">
                                    <button type="submit" class="btn btn-secondary">Print</button>
                                </div>
                            
                        </div>
                    </div>
                </div>
                <div class="col-md-6">
                <div class="print-layout-selection">
                <label for="print-layout">Select Print Layout:</label>
                <select id="print-layout">
                    <option value="default">Default</option>
                    <option value="5x2">5x2</option>
                    <option value="7x3">7x3</option> 
                    <option value="8x3">8x3</option> 
                    <option value="8x5">8x5</option> 
                </select>
                  <input type="hidden" id="printLayout" name="printLayout">
                </form>
            </div>
            </div>
            </div>
        </div>
    </div>
</section>

<!-- Bao gồm thư viện jQuery -->
<script src="https://code.jquery.com/jquery-3.6.0.min.js"></script>
 <script>
      $(document).ready(function() {
        // Lắng nghe sự kiện thay đổi của dropdown
        $('#print-layout').on('change', function() {
            // Lấy giá trị của dropdown được chọn
            var selectedLayout = $(this).val();
            var rows, cols;

            // Xác định số dòng và số cột tương ứng với layout được chọn
           if (selectedLayout === '5x2') {
            rows = 5;
            cols = 2;
        } else if (selectedLayout === '7x3') {
            rows = 7;
            cols = 3;
         } else if (selectedLayout === '8x3') {
            rows = 8;
            cols = 3;
        } else if (selectedLayout === '8x5') {
            rows = 8;
            cols = 5;
        } else {
            // Xử lý cho các tùy chọn khác nếu cần
        }

            // Cập nhật giá trị của trường ẩn printLayout
            $('#printLayout').val(rows + 'x' + cols);
        });
    });

    </script>
@endsection
