<div id="myModal" class="modal">
    <div class="modal-content">
        <span class="close">&times;</span>
        <h2>New Asset</h2>
        <div class="row">
            <div class="col-md-6">
                <!-- Form to create a new asset -->
                <form method="POST" action="{{ route('assets.store') }}" enctype="multipart/form-data">
                    @csrf
                    <!-- Asset Name -->
                    <div class="form-group">
                        <label for="name">Asset Name:</label>
                        <input type="text" id="name" name="name" class="form-control" placeholder="Enter asset name">
                    </div>

                    <!-- Notes -->
                    <div class="form-group">
                        <label for="notes">Notes:</label>
                        <textarea id="notes" name="notes" class="form-control" placeholder="Enter notes"></textarea>
                    </div>

                    <!-- Condition -->
                    <div class="form-group">
                        <label for="condition">Condition:</label>
                        <select id="condition" name="condition_id" class="form-control">
                            @foreach($conditions as $condition)
                            <option value="{{ $condition->id }}">{{ $condition->name }}</option>
                            @endforeach
                        </select>
                    </div>

                    <!-- Category -->
                    <div class="form-group">
                        <label for="category">Category:</label>
                        <select id="category" name="category_id" class="form-control">
                            @foreach($categories as $category)
                            <option value="{{ $category->id }}">{{ $category->name }}</option>
                            @endforeach
                        </select>
                    </div>

                    <!-- Location -->
                    <div class="form-group">
                        <label for="location">Location:</label>
                        <select id="location" name="location_id" class="form-control">
                            @foreach($locations as $location)
                            <option value="{{ $location->id }}">
                                {{ $location->name }} - {{ $location->department->name }} - {{  $location->department->address['city'] }}, {{  $location->department->address['state'] }}, {{  $location->department->address['country'] }}
                            </option>
                            @endforeach
                        </select>
                    </div>
            </div>
            <div class="col-md-6">
                <h4 class="text-center">Purchase Information</h4>

                <!-- Manufacturer -->
                <div class="form-group">
                    <label for="model">Manufacturer:</label>
                    <select id="manufacturer" name="manufacturer_id" class="form-control">
                        @foreach($manufacturers as $manufacturer)
                        <option value="{{ $manufacturer->id }}">{{ $manufacturer->name }}</option>
                        @endforeach
                    </select>
                </div>

                <!-- Model -->
                <div class="form-group">
                    <label for="model">Model:</label>
                    <select id="model" name="model_id" class="form-control">
                    </select>
                </div>

                <!-- Supplier -->
                <div class="form-group">
                    <label for="supplier">Supplier:</label>
                    <select id="supplier" name="supplier_id" class="form-control">
                        @foreach($suppliers as $supplier)
                        <option value="{{ $supplier->id }}">{{ $supplier->name }}</option>
                        @endforeach
                    </select>
                </div>

                <!-- Serial -->
                <div class="form-group">
                    <label for="serial">Serial:</label>
                    <input type="text" id="serial" name="serial" class="form-control" placeholder="Enter serial">
                </div>

                <!-- Date -->
                <div class="form-group">
                    <label for="date">Date:</label>
                    <input type="date" id="date" name="date" class="form-control">
                </div>

                <!-- Warranty -->
                <div class="form-group">
                    <label for="warranty">Warranty(month):</label>
                    <input type="text" id="warranty" name="warranty" class="form-control" placeholder="Enter warranty">
                </div>

                <!-- Price -->
                <div class="form-group">
                    <label for="price">Price:</label>
                    <input type="text" id="price" name="price" class="form-control" placeholder="Enter price">
                </div>
            </div>
        </div>
    <div class="row">
        <div class="col-md-12">
            <div class="form-group">
                <label for="images">Upload Images:</label>
                <div id="image-container">
                    <div id="large-preview">   
                        <span class="delete-image" onclick="removeThumbnail(this)">&#x1F5D1;</span>
                    </div>
                    <div id="thumbnails"></div>
                </div>
                <input type="file" id="images" name="images[]" class="form-control" multiple onchange="previewImages()">
                <div id="image-info" class="mt-2">0/0</div>
            </div>
        </div>
    </div>
        <button type="submit" class="btn btn-primary">Save</button>
        </form>
        <script>
            document.getElementById('manufacturer').addEventListener('change', function() {
                var manufacturerId = this.value;
                var modelSelect = document.getElementById('model');

                $.ajax({
                    url: '/models/by-manufacturer/' + manufacturerId,
                    type: 'GET',
                    success: function(response) {

                        modelSelect.innerHTML = '';
                        response.forEach(function(model) {
                            var option = document.createElement('option');
                            option.value = model.id;
                            option.text = model.name;
                            modelSelect.appendChild(option);
                        });
                    },
                    error: function(xhr) {

                        console.error(xhr.responseText);
                    }
                });
            });
        </script>
    </div>
</div>
