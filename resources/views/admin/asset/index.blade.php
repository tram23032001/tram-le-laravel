@extends('admin.layouts.admin')
@section('title', 'Manage Assets')
@section('content')
<section>
    <!-- Start content -->
    <div class="content">
        <div class="container">
            <div class="row">
                <div class="col-xs-6">
                    <div class="page-title-box">
                      <a href="{{ route('assets.index') }}"><h4 class="page-title">Asset</h4></a>
                    </div>
                </div>
                 <div class="col-xs-6">
                    <div class="page-title-box">
                     <a href="{{ route('assets.qrcode') }}"><h4 class="page-title">QR Codes</h4></a>
                    </div>
                </div>
            </div>
            <!-- end row -->
            <div class="row">
                <div class="col-sm-12">
                    <div class="card-box table-responsive">
                        <div class="row">
                            <div class="col-sm-6">
                                <div id="datatable_filter" class="dataTables_filter">
                                    <label>Search:<input type="search" class="form-control input-sm" placeholder="" aria-controls="datatable"></label>
                                </div>
                            </div>
                            <div class="col-sm-6 text-right">
                                <button class="btn btn-primary btn-sm" onclick="openPopup()">New Asset</button>
                                <!-- Include file popup blade -->
                                 @include('admin.asset.create')
                              <button class="btn btn-info btn-sm" onclick="openImportPopup()">Import Assets</button>
                                @include('admin.asset.import')
                            </div>
                        </div>
                        <table id="datatable-buttons" class="table table-striped table-colored table-info">
                            <thead>
                                <tr>
                                    <th style="width: 10%;">ID</th>
                                    <th style="width: 15%;">Name</th>
                                    <th style="width: 15%;">Condition</th>
                                    <th style="width: 15%;">Category</th>
                                    <th style="width: 15%;">Location</th>
                                    <th style="width: 15%;">Model</th>
                                    <th style="width: 15%;">Actions</th>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach($assets as $asset)
                                <tr>
                                    <td>{{ $asset->id }}</td>
                                    <td>{{ $asset->name }}</td>
                                    <td>{{ $asset->condition->name }}</td>
                                    <td>{{ $asset->category->name }}</td>
                                    <td>{{ $asset->location->name }}</td>
                                    <td>{{ $asset->model->name }}</td>
                                    <td>
                                        <button class="btn btn-primary btn-sm" onclick="openEditPopup({{ $asset->id }})">Edit</button>
                                         @include('admin.asset.edit', ['asset' => $asset])
                                        <button class="btn btn-info btn-sm">Copy</button>
                                        <form id="deleteForm" action="{{ route('assets.destroy', $asset->id) }}" method="POST" style="display: none;">
                                        @csrf
                                        @method('DELETE')
                                    </form>
                                       <button class="btn btn-danger btn-sm" onclick="deleteItemConfirmation('{{ $asset->id }}', 'asset')">Delete</button>
                                    </td>
                                </tr>
                                @endforeach
                            </tbody>
                        </table>
                          @if($assets->hasPages())
                                 <div class="pagination float-right margin-20">
                                    {{ $assets->appends($query = '')->links() }}
                                 </div>
                          @endif     
                    </div>
                </div>
            </div>
        </div> <!-- container -->
    </div> <!-- content -->
</section>
<!-- END HOME -->
@endsection
