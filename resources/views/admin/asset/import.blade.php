<div id="import-popup" class="modal">
    <div class="modal-content">
        <span class="close" onclick="closeImportPopup()">&times;</span>
        <h2>Import Assets</h2>
        <form method="POST" action="{{ route('assets.import') }}" enctype="multipart/form-data">
            @csrf
            <!-- File input for import -->
            <div class="form-group">
                <label for="import-file">Choose file:</label>
                <input type="file" id="file" name="file" class="form-control">
            </div>
            <!-- Submit button -->
            <button type="submit" class="btn btn-primary">Import</button>
        </form>
    </div>
</div>
