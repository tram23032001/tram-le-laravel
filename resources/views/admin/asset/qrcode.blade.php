@extends('admin.layouts.admin')
@section('title', 'Manage Assets')
@section('content')
<section>
    <!-- Start content -->
    <div class="content">
        <div class="container">
          <div class="row">
                <div class="col-xs-6">
                    <div class="page-title-box">
                      <a href="{{ route('assets.index') }}"><h4 class="page-title">Asset</h4></a>
                    </div>
                </div>
                 <div class="col-xs-6">
                    <div class="page-title-box">
                     <a href="{{ route('assets.qrcode') }}"><h4 class="page-title">QR Codes</h4></a>
                    </div>
                </div>
            </div>
            <div class="row">
            <div class="col-sm-12">
                    <div class="card-box table-responsive">
                        <div class="row">
                            <div class="col-sm-6">
                                <div id="datatable_filter" class="dataTables_filter">
                                    <label>Search:<input type="search" class="form-control input-sm" placeholder="" aria-controls="datatable"></label>
                                </div>
                            </div>
                           <div class="col-sm-6 text-right">
                                <a href="{{ route('assets.select') }}" class="btn btn-primary btn-sm">Print Labels</a>
                           </div>
                        </div>
        
                <div class="col-xs-6">
                   @foreach($assets as $asset)
                    <div class="card-box">
                        <p>ID: {{ $asset->id }}</p>
                        <p>Name: {{ $asset->name }}</p>
                        <p>Condition: {{ $asset->condition->name }}</p>
                        <p>Category: {{ $asset->category->name }}</p>
                        <p>Location: {{ $asset->location->name }}</p>
                        <p>Model: {{ $asset->model->name }}</p>
                        <p>Model: {{ $asset->model->name }}</p>
                    </div>
                @endforeach
                </div>
            
                <div class="col-xs-6">
                    @foreach($assets as $asset)
                        <div class="card-box">
                            <img src="{{ asset($asset->qr_code) }}" alt="QR Code" class="img-fluid qr-code">
                        </div>
                    @endforeach
                </div>

            </div>
        </div> <!-- container -->
    </div> <!-- content -->
     </div>
</section>
<!-- END HOME -->
@endsection
