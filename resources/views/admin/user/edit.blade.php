<!-- admin.user.edit.blade.php -->

<div id="editModal{{ $user->id }}" class="modal">
    <div class="modal-content">
        <span class="close">&times;</span>
        <h2>Edit User</h2>

        <!-- Form to edit a user -->
        <form method="POST" action="{{ route('admins.update', $user->id) }}">
            @csrf
            @method('PUT')

            <!-- User Name -->
            <div class="form-group">
                <label for="name">User Name:</label>
                <input type="text" id="name" name="name" class="form-control" value="{{ $user->name }}" placeholder="Enter user name">
            </div>

            <!-- Email -->
            <div class="form-group">
                <label for="email">Email:</label>
                <input type="email" id="email" name="email" class="form-control" value="{{ $user->email }}" placeholder="Enter email">
            </div>

            <!-- Phone -->
            <div class="form-group">
                <label for="phone">Phone:</label>
                <input type="text" id="phone" name="phone" class="form-control" value="{{ $user->phone }}" placeholder="Enter phone">
            </div>

            <!-- Roles -->
            <div class="form-group">
                <label for="roles">Roles:</label>
                <select id="roles" name="roles" class="form-control" required>
                   @foreach($roles as $role)
                        <option value="{{ $role->id }}" {{ $user->roles->contains($role->id) ? 'selected' : '' }}>
                            {{ $role->name }}
                        </option>
                    @endforeach
                </select>
            </div>

            <!-- Location -->
            <div class="form-group">
                <label for="location">Location:</label>
                <select id="location" name="location" class="form-control" required>
                    @foreach($locations as $location)
                        <option value="{{ $location->id }}" {{ $user->location_id == $location->id ? 'selected' : '' }}>
                            {{ $location->name }} - {{ $location->department->name }}
                        </option>
                    @endforeach
                </select>
            </div>

            <!-- Additional location fields -->
            <div class="location-fields" style="display: none;">
                <!-- Department -->
                <div class="form-group">
                    <label for="department">Department:</label>
                    <select id="department" name="department_id" class="form-control">
                        <!-- Departments will be loaded here -->
                    </select>
                </div>

                <!-- Floor -->
                <div class="form-group">
                    <label for="floor">Floor:</label>
                    <input type="text" id="floor" name="floor" class="form-control" readonly>
                </div>

                <!-- Unit -->
                <div class="form-group">
                    <label for="unit">Unit:</label>
                    <input type="text" id="unit" name="unit" class="form-control" readonly>
                </div>

                <!-- Building -->
                <div class="form-group">
                    <label for="building">Building:</label>
                    <input type="text" id="building" name="building" class="form-control" readonly>
                </div>

                <!-- Address -->
                <div class="form-group">
                    <div id="address"></div>
                </div>

                <!-- Zip Code -->
                <div class="form-group">
                    <label for="zip_code">Zip Code:</label>
                    <input type="text" id="zip_code" name="zip_code" class="form-control" readonly>
                </div>
            </div>

            <!-- Save button -->
            <button type="submit" class="btn btn-primary">Save Changes</button>
        </form>

        <!-- Script to display location information when selected -->
    </div>
</div>
