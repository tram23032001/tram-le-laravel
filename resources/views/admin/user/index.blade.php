@extends('admin.layouts.admin')
@section('title', '')
@section('content')
<section>
    <!-- Start content -->
    <div class="content">
        <div class="container">
            <div class="row">
            <div class="col-xs-6">
                <!-- User Section -->
                <div class="page-title-box">
                    <h4 class="page-title">Users</h4>
                </div>
                <!-- User Table Goes Here -->
            </div>
            <div class="col-xs-6">
                <!-- Role Section -->
                <div class="page-title-box">
                    <a href="{{ route('role.index') }}"><h4 class="page-title">Roles</h4></a>
                </div>
                <!-- Role Table Goes Here -->
            </div>
        </div>
            <!-- end row -->
            <div class="row">
                <div class="col-sm-12">
                    <div class="card-box table-responsive">
                        <div class="row">
                            <div class="col-sm-6">
                                <div id="datatable_filter" class="dataTables_filter">
                                    <label>Search:<input type="search" class="form-control input-sm" placeholder="" aria-controls="datatable"></label>
                                </div>
                            </div>
                            <div class="col-sm-6 text-right">
                              <ol class="breadcrumb p-0 m-0">
                            <li>
                                <a href="#">Zircos</a>
                            </li>
                            <li>
                                <a href="#">Tables </a>
                            </li>
                            <li class="active">
                                Datatable
                            </li>
                        </ol>
                        <div class="clearfix"></div>
                                <button class="btn btn-primary btn-sm" onclick="openPopup()">New User</button>
                                    @include('admin.user.create')
                                <!-- Include file popup blade -->
                                <button class="btn btn-info btn-sm">Import Users</button>
                            </div>
                        </div>
                        <table id="datatable-buttons" class="table table-striped table-colored table-info">
                            <thead>
                                <tr>
                                    <th style="width: 5%;">ID</th>
                                    <th style="width: 15%;">Name</th>
                                    <th style="width: 20%;">Email</th>
                                    <th style="width: 10%;">Phone</th>
                                    <th style="width: 10%;">Location</th>
                                    <th style="width: 20%;">Role</th>
                                    <th style="width: 20%;">Actions</th>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach($users as $user)
                                <tr>
                                    <td>{{ $user->id }}</td>
                                    <td>{{ $user->name }}</td>
                                    <td>{{ $user->email }}</td>
                                    <td>{{ $user->phone }}</td>
                                    <td>{{ $user->location ? $user->location->name : 'N/A' }}</td>
                                    <td>
                                        @if($user->userRole()->count() > 0)
                                            @foreach($user->userRole as $role)
                                                <span class="label label-success">{{ $role->name }}</span><br>
                                            @endforeach
                                        @else
                                            <span class="label label-warning">No Role Assigned</span>
                                        @endif
                                    </td>
                                    <td>
                                      <button class="btn btn-primary btn-sm" onclick="openEditPopup('{{ $user->id }}', 'user')">Edit User</button>
                                            @include('admin.user.edit', ['user' => $user])
                                        <button class="btn btn-info btn-sm">Copy</button>
                                    <form id="deleteForm" action="{{ route('admins.destroy', $user->id) }}" method="POST" style="display: none;">
                                        @csrf
                                        @method('DELETE')
                                    </form>
                                    <!-- Button để mở hộp thoại xác nhận xóa -->
                                    <button class="btn btn-danger btn-sm" onclick="deleteItemConfirmation('{{ $user->id }}', 'user')">Delete</button>

                                    </td>
                                </tr>
                                @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div> <!-- container -->
    </div> <!-- content -->
</section>
<!-- END HOME -->
@endsection
