<div id="myModal" class="modal">
  <div class="modal-content">
    <span class="close">&times;</span>
    <h2>New User</h2>
    
    <!-- Form to create a new user -->
    <form action="{{ route('admins.store') }}" method="POST">
        @csrf
        <div class="form-group">
            <label for="name">Name:</label>
            <input type="text" id="name" name="name" class="form-control" required>
        </div>
        <div class="form-group">
            <label for="email">Email:</label>
            <input type="email" id="email" name="email" class="form-control" required>
        </div>
        <div class="form-group">
            <label for="phone">Phone:</label>
            <input type="text" id="phone" name="phone" class="form-control" required>
        </div>
        <div class="form-group">
            <label for="roles">Roles:</label>
            <select id="roles" name="roles" class="form-control" required>
                @foreach($roles as $role)
                    <option value="{{ $role->id }}">{{ $role->name }}</option>
                @endforeach
            </select>
        </div>
        <div class="form-group">
            <label for="location">Location:</label>
            <select id="location" name="location" class="form-control" required>
                @foreach($locations as $location)
               <option value="{{ $location->id }}">{{ $location->name }} - {{ $location->department->name }}</option>
                @endforeach
            </select>
        </div>
        <div class="department-fields" style="display: none;">
            <div class="form-group" id="department-group">
                <label for="department">Department:</label>
                <select id="department" name="department" class="form-control" readonly>
                    <!-- Departments will be loaded here -->
                </select>
            </div>

            <div class="form-group">
                <label for="floor">Floor:</label>
                <input type="text" id="floor" name="floor" class="form-control" readonly>
            </div>

            <div class="form-group">
                <label for="unit">Unit:</label>
                <input type="text" id="unit" name="unit" class="form-control" readonly>
            </div>

            <div class="form-group">
                <label for="building">Building:</label>
                <input type="text" id="building" name="building" class="form-control" readonly>
            </div>
            
            <div class="form-group">
                <div id="address"></div>
            </div>
            
            <div class="form-group">
                <label for="zip_code">Zip Code:</label>
                <input type="text" id="zip_code" name="zip_code" class="form-control" readonly>
            </div>
        </div>

        <button type="submit" class="btn btn-primary">Submit</button>
    </form>
  </div>
</div>

