<!-- Bootstrap -->
<script src="{{ asset('../admin/js/bootstrap.min.js') }}"></script>
<script src="{{ asset('../admin/js/jquery.min.js') }}"></script>
<script src="{{ asset('../admin/js/bootstrap.min.js') }}"></script>
<script src="{{ asset('../admin/js/detect.js') }}"></script>
<script src="{{ asset('../admin/js/fastclick.js') }}"></script>
<script src="{{ asset('../admin/js/jquery.blockUI.js') }}"></script>
<script src="{{ asset('../admin/js/waves.js') }}"></script>
<script src="{{ asset('../admin/js/jquery.slimscroll.js') }}"></script>
<script src="{{ asset('../admin/js/jquery.scrollTo.min.js') }}"></script>

    <!-- App js -->
<script src="{{ asset('../admin/js/jquery.core.js') }}"></script>
<script src="{{ asset('../admin/js/jquery.app.js') }}"></script>
<script src="{{ asset('../admin/js/popup.js') }}"></script>

<!-- jQuery BlockUI -->
<script src="{{ asset('../admin/js/jquery.blockUI.js') }}"></script>
<script src="{{ asset('../admin/js/jquery.scrollTo.min.js') }}"></script>

<!-- Wow -->
<script src="{{ asset('../admin/js/wow.min.js') }}"></script>
<!-- Linking JavaScript files from directory ../admin/pages/ -->
<script src="{{ asset('../admin/js/image.js') }}"></script>

<script src="{{ asset('../admin/pages/jquery.autocomplete.init.js') }}"></script>
<script src="{{ asset('../admin/pages/jquery.blog-add.init.js') }}"></script>
<script src="{{ asset('../admin/pages/jquery.blog-dashboard.js') }}"></script>
<script src="{{ asset('../admin/pages/jquery.c3-chart.init.js') }}"></script>
<script src="{{ asset('../admin/pages/jquery.chartist.init.js') }}"></script>
<script src="{{ asset('../admin/pages/jquery.chartjs.init.js') }}"></script>
<script src="{{ asset('../admin/pages/jquery.charts-sparkline.js') }}"></script>
<script src="{{ asset('../admin/pages/jquery.chat.js') }}"></script>
<script src="{{ asset('../admin/pages/jquery.dashboard.js') }}"></script>
<script src="{{ asset('../admin/pages/jquery.dashboard_2.js') }}"></script>
<script src="{{ asset('../admin/pages/jquery.datatables.editable.init.js') }}"></script>
<script src="{{ asset('../admin/pages/jquery.datatables.init.js') }}"></script>
<script src="{{ asset('../admin/pages/jquery.fileuploads.init.js') }}"></script>
<script src="{{ asset('../admin/pages/jquery.flot.init.js') }}"></script>
<script src="{{ asset('../admin/pages/jquery.form-advanced.init.js') }}"></script>
<script src="{{ asset('../admin/pages/jquery.form-pickers.init.js') }}"></script>
<script src="{{ asset('../admin/pages/jquery.fullcalendar.js') }}"></script>
<script src="{{ asset('../admin/pages/jquery.gmaps.js') }}"></script>
<script src="{{ asset('../admin/pages/jquery.google-charts.init.js') }}"></script>
<script src="{{ asset('../admin/pages/jquery.icons.js') }}"></script>
<script src="{{ asset('../admin/pages/jquery.jvectormap.init.js') }}"></script>
<script src="{{ asset('../admin/pages/jquery.mapael-map.init.js') }}"></script>
<script src="{{ asset('../admin/pages/jquery.morris.init.js') }}"></script>
<script src="{{ asset('../admin/pages/jquery.nestable.init.js') }}"></script>
<script src="{{ asset('../admin/pages/jquery.property-add.init.js') }}"></script>
<script src="{{ asset('../admin/pages/jquery.range-sliders.js') }}"></script>
<script src="{{ asset('../admin/pages/jquery.rating.js') }}"></script>
<script src="{{ asset('../admin/pages/jquery.real-estate-dashboard.js') }}"></script>
<script src="{{ asset('../admin/pages/jquery.sweet-alert.init.js') }}"></script>
<script src="{{ asset('../admin/pages/jquery.toastr.js') }}"></script>
<script src="{{ asset('../admin/pages/jquery.todo.js') }}"></script>
<script src="{{ asset('../admin/pages/jquery.tooltipster.js') }}"></script>
<script src="{{ asset('../admin/pages/jquery.wizard-init.js') }}"></script>
<script src="https://code.jquery.com/jquery-3.6.0.min.js"></script>
<script src="https://cdn.jsdelivr.net/npm/sweetalert2@10"></script>

<!-- <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.2.2/dist/js/bootstrap.bundle.min.js" ></script> -->

  <!-- jQuery  -->
