<!-- Components -->
<link rel="stylesheet" type="text/css" href="{{ asset('../admin/css/components.css') }}">

<!-- Core -->
<link rel="stylesheet" type="text/css" href="{{ asset('../admin/css/core.css') }}">

<!-- Elements -->
<link rel="stylesheet" type="text/css" href="{{ asset('../admin/css/elements.css') }}">

<!-- Icons -->
<link rel="stylesheet" type="text/css" href="{{ asset('../admin/css/icons.css') }}">

<!-- Menu -->
<link rel="stylesheet" type="text/css" href="{{ asset('../admin/css/menu.css') }}">

<!-- Pages -->
<link rel="stylesheet" type="text/css" href="{{ asset('../admin/css/pages.css') }}">

<!-- Responsive -->
<link rel="stylesheet" type="text/css" href="{{ asset('../admin/css/responsive.css') }}">

<!-- Typicons -->
<link rel="stylesheet" type="text/css" href="{{ asset('../admin/css/typicons.css') }}">

<!-- Variables -->
<link rel="stylesheet" type="text/css" href="{{ asset('../admin/css/variables.css') }}">
  <link href="{{ asset('../admin/css/bootstrap.min.css') }}" rel="stylesheet" type="text/css" />
    <link href="{{ asset('../admin/css/core.css') }}" rel="stylesheet" type="text/css" />
    <link href="{{ asset('../admin/css/components.css') }}" rel="stylesheet" type="text/css" />
    <link href="{{ asset('../admin/css/icons.css') }}" rel="stylesheet" type="text/css" />
    <link href="{{ asset('../admin/css/pages.css') }}" rel="stylesheet" type="text/css" />
    <link href="{{ asset('../admin/css/menu.css') }}" rel="stylesheet" type="text/css" />
    <link href="{{ asset('../admin/css/responsive.css') }}" rel="stylesheet" type="text/css" />
    <link rel="stylesheet" href="{{ asset('../admin/plugins/switchery/switchery.min.css')}}">
    <!-- Additional CSS -->
    <link rel="stylesheet" type="text/css" href="{{ asset('../admin/css/icons.css') }}">
    <script src="{{ asset('../admin/js/modernizr.min.js') }}"></script>
<!-- Font Awesome -->
<link rel="stylesheet" type="text/css" href="{{ asset('../admin/fonts/fontawesome-webfont.eot') }}">
<link rel="stylesheet" type="text/css" href="{{ asset('../admin/fonts/fontawesome-webfont.svg') }}">
<link rel="stylesheet" type="text/css" href="{{ asset('../admin/fonts/fontawesome-webfont.ttf') }}">
<link rel="stylesheet" type="text/css" href="{{ asset('../admin/fonts/fontawesome-webfont.woff') }}">
<link rel="stylesheet" type="text/css" href="{{ asset('../admin/fonts/fontawesome-webfont.woff2') }}">
<link rel="stylesheet" type="text/css" href="{{ asset('../admin/fonts/FontAwesome.otf') }}">

<!-- Glyphicons Halflings Regular -->
<link rel="stylesheet" type="text/css" href="{{ asset('../admin/fonts/glyphicons-halflings-regular.eot') }}">
<link rel="stylesheet" type="text/css" href="{{ asset('../admin/fonts/glyphicons-halflings-regular.svg') }}">
<link rel="stylesheet" type="text/css" href="{{ asset('../admin/fonts/glyphicons-halflings-regular.ttf') }}">
<link rel="stylesheet" type="text/css" href="{{ asset('../admin/fonts/glyphicons-halflings-regular.woff') }}">
<link rel="stylesheet" type="text/css" href="{{ asset('../admin/fonts/glyphicons-halflings-regular.woff2') }}">

<!-- Ionicons -->
<link rel="stylesheet" type="text/css" href="{{ asset('../admin/fonts/ionicons.eot') }}">
<link rel="stylesheet" type="text/css" href="{{ asset('../admin/fonts/ionicons.svg') }}">
<link rel="stylesheet" type="text/css" href="{{ asset('../admin/fonts/ionicons.ttf') }}">
<link rel="stylesheet" type="text/css" href="{{ asset('../admin/fonts/ionicons.woff') }}">

<!-- Material Design Icons -->
<link rel="stylesheet" type="text/css" href="{{ asset('../admin/fonts/materialdesignicons-webfont.eot') }}">
<link rel="stylesheet" type="text/css" href="{{ asset('../admin/fonts/materialdesignicons-webfont.svg') }}">
<link rel="stylesheet" type="text/css" href="{{ asset('../admin/fonts/materialdesignicons-webfont.ttf') }}">
<link rel="stylesheet" type="text/css" href="{{ asset('../admin/fonts/materialdesignicons-webfont.woff') }}">
<link rel="stylesheet" type="text/css" href="{{ asset('../admin/fonts/materialdesignicons-webfont.woff2') }}">

<!-- Themify -->
<link rel="stylesheet" type="text/css" href="{{ asset('../admin/fonts/themify.eot') }}">
<link rel="stylesheet" type="text/css" href="{{ asset('../admin/fonts/themify.svg') }}">
<link rel="stylesheet" type="text/css" href="{{ asset('../admin/fonts/themify.ttf') }}">
<link rel="stylesheet" type="text/css" href="{{ asset('../admin/fonts/themify.woff') }}">

<!-- Typicons -->
<link rel="stylesheet" type="text/css" href="{{ asset('../admin/fonts/typicons.eot') }}">
<link rel="stylesheet" type="text/css" href="{{ asset('../admin/fonts/typicons.less') }}">
<link rel="stylesheet" type="text/css" href="{{ asset('../admin/fonts/typicons.svg') }}">
<link rel="stylesheet" type="text/css" href="{{ asset('../admin/fonts/typicons.ttf') }}">
<link rel="stylesheet" type="text/css" href="{{ asset('../admin/fonts/typicons.woff') }}">

<script src="{{ asset('../admin/js/modernizr.min.js') }}"></script>
<!-- <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-EVSTQN3/azprG1Anm3QDgpJLIm9Nao0Yz1ztcQTwFspd3yD65VohhpuuCOmLASjC" crossorigin="anonymous"> -->
