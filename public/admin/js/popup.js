// Lấy tham chiếu đến popup và nút đóng
var modal = document.getElementById("myModal");
var closeBtn = document.getElementsByClassName("close")[0];

// Mở popup
function openPopup() {
  modal.style.display = "block";
}

// Đóng popup khi nhấn nút đóng
closeBtn.onclick = function() {
  modal.style.display = "none";
}

window.onclick = function(event) {
  if (event.target == modal) {
    modal.style.display = "none";
  }
}

function deleteItemConfirmation(itemId, itemType) {
  Swal.fire({
      title: 'Confirmation',
      text: 'Are you sure you want to delete this ' + itemType + '?',
      icon: 'warning',
      showCancelButton: true,
      confirmButtonText: 'Yes',
      cancelButtonText: 'No'
  }).then((result) => {
      if (result.isConfirmed) {
          // Submit the form
          document.getElementById('deleteForm').submit();
      }
  });
}


function openEditPopup(id, type) {
  // Xây dựng id của modal dựa vào id và type được cung cấp
  var modalId = 'editModal' + id;

  // Tìm modal dựa vào id được xây dựng
  var editModal = document.getElementById(modalId);

  // Kiểm tra xem modal có tồn tại không
  if (editModal) {
    // Hiển thị modal
    editModal.style.display = 'block';

    // Tìm nút đóng trong modal
    var closeEditBtn = editModal.querySelector(".close");

    // Xác định hành động khi nút đóng được nhấn
    closeEditBtn.onclick = function() {
      editModal.style.display = "none"; // Đóng modal
    }
  } else {
    // Nếu không tìm thấy modal, xuất thông báo lỗi
    console.error('Invalid type provided');
  }
}

function openImportPopup() {
  document.getElementById('import-popup').style.display = 'block';
}

function closeImportPopup() {
  document.getElementById('import-popup').style.display = 'none';
}

