function previewImages() {
    var largePreview = document.querySelector('#large-preview');
    var thumbnails = document.querySelector('#thumbnails');
    var files = document.querySelector('input[type=file]').files;
    var imageInfo = document.querySelector('#image-info');

    function readAndPreview(file, index) {
        if ( /\.(jpe?g|png|gif)$/i.test(file.name) ) {
            var reader = new FileReader();

            reader.addEventListener("load", function () {
                var image = new Image();
                image.title = file.name;
                image.src = this.result;

                largePreview.innerHTML = '';
                largePreview.appendChild(image);

                var thumbnail = document.createElement('div');
                thumbnail.classList.add('thumbnail');
                thumbnail.dataset.index = index; // Set data-index attribute for thumbnail
                thumbnail.onclick = function() {
                    largePreview.innerHTML = '';
                    largePreview.appendChild(image);
                };
                var img = document.createElement('img');
                img.src = this.result;
                thumbnail.appendChild(img);

                var deleteIcon = document.createElement('span');
                deleteIcon.classList.add('delete-image');
                deleteIcon.innerHTML = '&#x1F5D1;'; 
                deleteIcon.onclick = function() {
                    var parent = thumbnail.parentElement;
                    thumbnail.remove();
                    var index = parseInt(thumbnail.dataset.index);
                    var largeImage = document.querySelector('#large-preview img[data-index="' + index + '"]');
                    if (largeImage) {
                        largeImage.remove();
                    }
                    updateImageInfo();
                };
                thumbnail.appendChild(deleteIcon);

                largePreview.appendChild(image);
                thumbnails.appendChild(thumbnail);

                // Cập nhật thông tin số thứ tự ảnh
                var currentIndex = thumbnails.children.length;
                imageInfo.textContent = currentIndex + '/' + files.length;
            });

            reader.readAsDataURL(file);
        }
    }

    if (files) {
        [].forEach.call(files, readAndPreview);
    }
    
}

function updateImageInfo() {
    var thumbnails = document.querySelector('#thumbnails');
    var imageInfo = document.querySelector('#image-info');
    var currentIndex = thumbnails.children.length;
    var files = document.querySelector('input[type=file]').files;
    imageInfo.textContent = currentIndex + '/' + files.length;
}

function removeThumbnail(element) {
    var thumbnail = element.parentElement;
    var largePreview = document.querySelector('#large-preview');
    var index = parseInt(thumbnail.dataset.index);

    // Xóa hình nhỏ
    thumbnail.remove(); 

    // Tìm hình lớn tương ứng và xóa
    var largeImage = document.querySelector('#large-preview img[data-index="' + index + '"]');
    if (largeImage) {
        largeImage.remove();
    }
    
    updateImageInfo();
}
