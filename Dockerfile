# Sử dụng một hình ảnh cơ sở có chứa PHP
FROM php:8.1.6-apache

# Thiết lập thư mục làm việc
WORKDIR /var/www/html

# Sao chép mã nguồn ứng dụng vào thư mục làm việc trong hình ảnh
COPY . .

# Thiết lập quyền cho thư mục storage và cache
RUN chmod -R 777 storage bootstrap/cache

# Cài đặt các phụ thuộc của ứng dụng PHP sử dụng Composer
RUN apt-get update && apt-get install -y \
    git \
    unzip \
    && apt-get clean && rm -rf /var/lib/apt/lists/* \
    && curl -sS https://getcomposer.org/installer | php -- --install-dir=/usr/local/bin --filename=composer

# Cài đặt các phụ thuộc PHP bổ sung thông qua Composer
RUN composer install --no-dev

# Thiết lập môi trường
ENV APACHE_DOCUMENT_ROOT /var/www/html/public
RUN sed -ri -e 's!/var/www/html!${APACHE_DOCUMENT_ROOT}!g' /etc/apache2/sites-available/*.conf
RUN sed -ri -e 's!/var/www/!${APACHE_DOCUMENT_ROOT}!g' /etc/apache2/apache2.conf /etc/apache2/conf-available/*.conf

# Khai báo cổng cho Apache
EXPOSE 80

# Lệnh để khởi chạy máy chủ Apache khi container được khởi chạy
CMD ["apache2-foreground"]
