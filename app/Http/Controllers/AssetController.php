<?php

namespace App\Http\Controllers;

use App\Services\AssetService;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Request;
use Illuminate\View\View;
use App\Models\Location;
use App\Models\Condition;
use App\Models\Asset;
use App\Models\Image;
use App\Models\AssetModel;
use App\Models\Manufacturer;
use App\Models\Supplier;
use App\Models\Category;
use App\Exports\AssetsExport;
use App\Imports\AssetsImport;
use Maatwebsite\Excel\Facades\Excel;
use Illuminate\Pagination\Paginator;
use BaconQrCode\Encoder\QrCode;
use BaconQrCode\Common\Mode;
use BaconQrCode\Renderer\ImageRenderer;
use BaconQrCode\Writer;
use Illuminate\Http\Response;
use BaconQrCode\Renderer\Image\ImagickImageBackEnd;
use BaconQrCode\Renderer\RendererStyle\RendererStyle;

class AssetController extends Controller
{
    protected $assetService;

    public function __construct(AssetService $assetService)
    {
        $this->assetService = $assetService;
    }

    public function index(Request $request): View
    {
        try {
            $assets = $this->assetService->getAllAssets(NUMBER_PAGINATION);
            $locations = Location::all();
            $suppliers = Supplier::all();
            $categories = Category::all();
            $conditions = Condition::all();
            $models = AssetModel::all();
            $manufacturers = Manufacturer::all();
            return view('admin.asset.index', compact('assets', 'locations', 'categories', 'conditions', 'manufacturers', 'suppliers', 'models'));
           
        } catch (\Exception $e) {
            return view('errors.404')->withErrors(['message' => 'Failed to load assets.']);
        }
    }

    public function qrcode()
    {
        $assets = $this->assetService->getAllAssets(NUMBER_PAGINATION);

        return view('admin.asset.qrcode', compact('assets'));
    }

    public function create(): View
    {
        return view('admin.asset.create');
    }

        public function store(Request $request): RedirectResponse
    {
      
        $asset = $this->assetService->createAsset($request->all());

        $assetId = $asset->id;
        $qrData = json_encode($asset->formatQRData());

        $renderer = new ImageRenderer(
            new RendererStyle(256), 
            new ImagickImageBackEnd()
        );
        $writer = new Writer($renderer);

        $qrCode = $writer->writeFile($qrData, 'qrcodes/' . $assetId . '.png');

        $asset->qr_code = 'qrcodes/' . $assetId . '.png';
        $asset->save();

        return redirect()->route('assets.index')->with('success', 'Asset created successfully.');
    }


    public function edit(string $id): View
    {
        $asset = $this->assetService->getAssetById($id);
        return view('admin.asset.edit', compact('asset'));
    }

    public function update(Request $request, string $id): RedirectResponse
    {
    
        $data = $request->all(); 
        
        $this->assetService->updateAsset($id, $data);

        $updatedAsset = Asset::findOrFail($id);

        $qrData = json_encode($updatedAsset->formatQRData());

        $renderer = new ImageRenderer(
            new RendererStyle(256), 
            new ImagickImageBackEnd()
        );
        $writer = new Writer($renderer);

        $qrCode = $writer->writeFile($qrData, 'qrcodes/' . $id . '.png');

        $updatedAsset->qr_code = 'qrcodes/' . $id . '.png';
        $updatedAsset->save();

        return redirect()->route('assets.index')->with('success', 'Asset updated successfully.');
    }

    public function destroy(string $id): RedirectResponse
    {
        $this->assetService->deleteAsset($id);
        return redirect()->route('assets.index')->with('success', 'Asset deleted successfully.');
    }

    public function import()
    {
        try {
            Excel::import(new AssetsImport, request()->file('file'));
            return back()->with('success', 'Dữ liệu đã được import thành công.');
        } catch (\Exception $e) {
         
            return back()->with('error', 'Đã xảy ra lỗi: ' . $e->getMessage());
        }
    }

    public function selectAssets()
    {
       $assets = $this->assetService->getAllAssets(NUMBER_PAGINATION);

        return view('admin.asset.select', compact('assets'));
    }

    public function printTemplate(Request $request)
{
    // Nhận danh sách các tài sản đã chọn từ yêu cầu POST
    $selectedAssets = $request->input('selectedAssets');
    
    // Chuyển chuỗi thành mảng bằng cách tách chuỗi bằng dấu phẩy
    $selectedAssetsArray = explode(',', $selectedAssets);

    // Truyền danh sách tài sản đã chọn vào view template
    return view('admin.asset.print_template', ['selectedAssets' => $selectedAssetsArray]);
}



    public function printAssets(Request $request)
    {
         // Nhận số dòng và số cột từ request
         $printLayout = $request->input('printLayout');

        // Giải mã số dòng và số cột từ chuỗi nhận được
        list($rows, $cols) = explode('x', $printLayout);
                // Nhận danh sách các ID đã chọn từ form
        $selectedIdsString = $request->input('selectedAssets');

        // Loại bỏ dấu ngoặc đơn và dấu ngoặc kép từ chuỗi JSON
        $selectedIdsString = str_replace(['[', ']', '"', "'"], '', $selectedIdsString);

        // Chuyển chuỗi thành mảng các ID
        $selectedIds = explode(',', $selectedIdsString);

        // Nhận danh sách các trường được chọn từ dropdown
        $selectedFields = $request->input('selectedFields');

        // Chọn các trường cần lấy từ cơ sở dữ liệu
        $query = Asset::whereIn('id', $selectedIds);
        
        // Chọn các trường được chọn từ dropdown
        foreach ($selectedFields as $field) {
            $query->addSelect($field);
        }

        //  // Truy vấn cơ sở dữ liệu để lấy thông tin chi tiết của các tài sản dựa trên các ID đã chọn
        $selectedAssets = Asset::whereIn('id', $selectedIds)
            ->with('location.department') // Sử dụng eager loading để lấy thông tin của location và department
            ->get();


        // Trả về view với mảng các đối tượng tài sản
        return view('admin.asset.print_result', compact('selectedAssets','selectedFields','rows','cols'));
    }

}