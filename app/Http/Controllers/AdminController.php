<?php
namespace App\Http\Controllers;

use App\Services\UserService;
use App\Models\User;
use Illuminate\Support\Facades\Mail;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Illuminate\View\View;
use App\Http\Requests\UserRequest;
use App\Models\Category;
use Illuminate\Support\Facades\Log;
use Illuminate\Pagination\Paginator;
use App\Mail\OrderShipped;
use Illuminate\Http\Response;
use App\Models\Role;
use App\Models\Location;
use Illuminate\Support\Str;
use Symfony\Component\Mime\Part\TextPart;
use Illuminate\Support\Facades\DB;
class AdminController extends Controller
{
    protected $userService;

    public function __construct(UserService $userService)
    {
        $this->userService = $userService;
    }

    public function index(Request $request): View
    {
        try {
           
            $users = $this->userService->getAllUsersPaginated(NUMBER_PAGINATION);
            
            $roles = Role::all();
            $locations = Location::all();
            
            return view('admin.user.index', compact('users', 'roles', 'locations'));
        } catch (\Exception $e) {
            return view('errors.404');
        }
    }


    public function create(): View
    {
        return view('admin.users.create');
    }

    public function store(Request $request): RedirectResponse
{
    DB::beginTransaction();

    try {
       
        $requestData = $request->all();
        $randomPassword = Str::random(10);
        Mail::send('mail.auth.password', ['password' => $randomPassword], function ($message) use ($request) {
            $message->subject('Your New Password');
            $message->to($request->input('email'));
        });
        $requestData['password'] = Hash::make($randomPassword);
        $user = $this->userService->createUser($requestData);

        DB::commit();

        return redirect()->route('admins.index')->with('success', 'User created successfully. Random password has been sent to the email.');
    } catch (\Exception $e) {
        DB::rollback();
        return redirect()->back()->with('error', 'An error occurred while creating the user.');
    }
}
    public function edit(string $id): View
    {
        $user = $this->userService->getUserById($id);
        return view('admin.user.edit', compact('user'));
    }

    public function update(Request $request, string $id): RedirectResponse
    {

        $data = $request->all(); 
        $this->userService->updateUser($id, $data);
        return redirect()->route('admins.index')->with('success', 'User updated successfully.');
    }

    public function destroy(string $id): RedirectResponse
    {
        $this->userService->deleteUser($id);
        return redirect()->route('admins.index')->with('success', 'User deleted successfully.');
    }

    public function response()
    {
        return response('Hello World', 200)
            ->header('Content-Type', 'text/plain');
    }

    
    public function downloadFile()
    {
        $pathToFile = public_path('files/file.pdf'); 
        $name = 'file.pdf'; 

        return response()->download($pathToFile, $name);
    }
    

      


    
  }
