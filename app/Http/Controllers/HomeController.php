<?php
namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use Illuminate\Http\JsonResponse;
use Illuminate\View\View;
use App\Models\User;
use App\Models\Asset;

class HomeController extends Controller
{
    public function index(): View
    {
        $recentUsers = User::orderBy('id', 'desc')->take(5)->get();
        $recentAssets = Asset::orderBy('id', 'desc')->take(5)->get();
        
        return view('admin.home.index', compact('recentUsers', 'recentAssets'));
    }
}
