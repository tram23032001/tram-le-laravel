<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\AssetModel; 

class ModelController extends Controller
{
    public function getModelsByManufacturer($manufacturerId)
    {
        $models = AssetModel::where('manufacturer_id', $manufacturerId)->get();

        return response()->json($models); 
    }
}
