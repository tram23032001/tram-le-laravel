<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\SendsPasswordResetEmails;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Password; 
use Illuminate\Http\Request;
use App\Models\User;
class ForgotPasswordController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Password Reset Controller
    |--------------------------------------------------------------------------
    |
    | This controller is responsible for handling password reset emails and
    | includes a trait which assists in sending these notifications from
    | your application to your users. Feel free to explore this trait.
    |
    */

    use SendsPasswordResetEmails;

    public function showLinkRequestForm()
    {
        return view('admin.auth.passwords.forgot');
    }

  
    public function sendResetLinkEmail(Request $request)
    {
        $this->validateEmail($request);
    
        $user = User::where('email', $request->email)->first();
    
        if (!$user || $user->email_verified_at === null) {
    
            return redirect()->route('admin.register')->with('error', 'Email chưa có tài khoản. Vui lòng đăng ký tài khoản mới.');
        }
    
        $response = Password::broker('users')->sendResetLink(
            $request->only('email')
        );
    
        return $response == Password::RESET_LINK_SENT
            ? back()->with('status', trans($response))
            : back()->withErrors(['email' => trans($response)]);
    }

}
