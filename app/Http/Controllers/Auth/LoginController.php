<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\User;
use App\Providers\RouteServiceProvider;
use Illuminate\Support\Facades\Auth;
use Laravel\Sanctum\HasApiTokens;
use Illuminate\Support\Facades\Session;
class LoginController extends Controller
{

    public function __construct(User $user)
    {
        $this->middleware('guest')->except('logout');
        $this->user = $user;
    }
    public function showLoginForm()
    {
        return view('admin.auth.login');
    }

    public function login(Request $request)
    {
        $credentials = $request->only('email', 'password');

        if (Auth::attempt($credentials)) {
        
            $user = Auth::user();
            $token = $user->createToken('authToken')->plainTextToken;
            Session::put('authToken', $token);
            $request->session()->flash('success', 'Bạn đã đăng nhập thành công.');
            return redirect()->route('admin.home');
        }

        // Đăng nhập không thành công
        return redirect()->route('admin.login')->withErrors(['email' => 'Thông tin đăng nhập không hợp lệ.']);
    }

        public function logout(Request $request)
    {
        $request->session()->invalidate(); 
        $request->session()->regenerateToken();
        
        Auth::logout();

        // Chuyển hướng về trang đăng nhập sau khi đăng xuất
        return redirect()->route('admin.login');
    }

}
