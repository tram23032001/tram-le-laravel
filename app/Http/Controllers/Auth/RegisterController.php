<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\Models\User;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Mail;
use App\Mail\VerificationCodeMail;
use App\Http\Requests\RegisterRequest;
use App\Jobs\SendVerificationCodeEmail;

class RegisterController extends Controller
{
    public function register()
    {
        if (Auth::guard('users')->check()) {
            return redirect()->back();
        }

        return view('admin.auth.register');
    }

    public function postRegister(Request $request)
{
  
    $data = $request->all();

    $existingUser = User::where('email', $data['email'])->first();

    if ($existingUser) {
        $code = rand(100000, 999999);
        $existingUser->verification_code = $code;
        $existingUser->save();

        SendVerificationCodeEmail::dispatch($existingUser, $code);
        
        
        return redirect()->route('verification.show')->with('success', 'Please check your email for verification code.');
    } else {
      
        $user = User::create([
            'name' => $data['name'],
            'email' => $data['email'],
            'password' => Hash::make($data['password']),
        ]);

        $code = rand(100000, 999999);

      
        $user->verification_code = $code;
        $user->save();

        SendVerificationCodeEmail::dispatch($user, $code);
        $request->session()->put('email', $user->email);
        return redirect()->route('verification.show')->with('success', 'Please check your email for verification code.');
    }
}



}
