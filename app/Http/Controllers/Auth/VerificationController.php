<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use App\Providers\RouteServiceProvider;
use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Http\Request;
use App\Models\User;

class VerificationController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Email Verification Controller
    |--------------------------------------------------------------------------
    |
    | This controller is responsible for handling email verification for any
    | user that recently registered with the application. Emails may also
    | be re-sent if the user didn't receive the original email message.
    |
    */



    /**
     * Where to redirect users after verification.
     *
     * @var string
     */


    /**
     * Create a new controller instance.
     *
     * @return void
     */
  

     public function showVerificationForm(Request $request)
    {
        $email = $request->session()->get('email');
        return view('admin.auth.verify', ['email' => $email]);
    }



   
    public function verify(Request $request)
    {

        $code = $request->verification_code;

    
        $user = User::where('verification_code', $code)->first();

        if ($user) {
            $user->email_verified_at = now();
            $user->verification_code = null;
            $user->save();

            return redirect('/login')->with('success', 'Địa chỉ email đã được xác minh thành công!');
        } else {
            return redirect('/verify-code')->with('error', 'Invalid verification code. Please try again.');
        }
    }

}
