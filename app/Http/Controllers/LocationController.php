<?php

namespace App\Http\Controllers;

use App\Services\LocationService;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Request;
use Illuminate\View\View;
use App\Models\Location;
use Illuminate\Pagination\Paginator;
use App\Models\Department;
use Maatwebsite\Excel\Facades\Excel;
use App\Imports\LocationsImport;

class LocationController extends Controller
{
    protected $locationService;

    public function __construct(LocationService $locationService)
    {
        $this->locationService = $locationService;
    }

    public function index(Request $request): View
    {
        try {
            $locations = $this->locationService->getAllLocations(NUMBER_PAGINATION);
            $departments = Department::all();
            return view('admin.location.index', compact('locations','departments'));
        } catch (\Exception $e) {
            return view('errors.404')->withErrors(['message' => 'Failed to load locations.']);
        }
    }

    public function create(): View
    {
        return view('admin.location.create');
    }

    public function store(Request $request): RedirectResponse
    {

        $this->locationService->createLocation($request->all());
        return redirect()->route('locations.index')->with('success', 'Location created successfully.');
    }

    public function edit(string $id): View
    {
        $location = $this->locationService->getLocationById($id);
        return view('admin.locations.edit', compact('location'));
    }

    public function update(Request $request, string $id): RedirectResponse
    {
        $data = $request->all(); 
        $this->locationService->updateLocation($id, $data);

        return redirect()->route('locations.index')->with('success', 'Location updated successfully.');
    }


    public function destroy(string $id): RedirectResponse
    {
        $this->locationService->deleteLocation($id);
        return redirect()->route('locations.index')->with('success', 'Location deleted successfully.');
    }


    public function import()
    {
        try {
            Excel::import(new LocationsImport, request()->file('file'));
            
            return back()->with('success', 'Dữ liệu đã được import thành công.');
        } catch (\Exception $e) {
        
            return back()->with('error', 'Đã xảy ra lỗi: ' . $e->getMessage());
        }
    }
}
