<?php

namespace App\Http\Controllers;

use App\Services\RoleService;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Request;
use Illuminate\View\View;
use App\Models\Role;
use App\Models\GroupPermission;
class RoleController extends Controller
{
    protected $roleService;

    public function __construct(RoleService $roleService)
    {
        $this->roleService = $roleService;
    }

    public function index(Request $request): View
    {
        try {
            $roles = $this->roleService->getAllRoles(NUMBER_PAGINATION);
            return view('admin.role.index', compact('roles'));
        } catch (\Exception $e) {
            return view('errors.404')->withErrors(['message' => 'Failed to load roles.']);
        }
    }

    public function create(): View
    {
        $permissionGroups = GroupPermission::select('*')->with('permissions')->get();
        return view('admin.role.create', compact('permissionGroups'));
    }
    public function store(Request $request): RedirectResponse
    {
        \DB::beginTransaction(); 
        try {
        
            $role = $this->roleService->createRole($request->all());

            if (!empty($request->permissions)) {
                $existingPermissions = \DB::table('permission_role')
                    ->where('role_id', $role->id)
                    ->pluck('permission_id')
                    ->toArray();

                foreach ($request->permissions as $permission) {
                    if (!in_array($permission, $existingPermissions)) {
                        \DB::table('permission_role')->insert(['permission_id' => $permission, 'role_id' => $role->id]);
                    }
                }
            }

            \DB::commit();
            return redirect()->route('role.index')->with('success', 'Role created successfully.');
        } catch (\Exception $exception) {
            \DB::rollBack(); 
            return redirect()->back()->with('error', 'Failed to create role: ' . $exception->getMessage());
        }
    }


    public function edit(string $id): View
    {
        $role = $this->roleService->getRoleById($id);
        $listPermission = \DB::table('permission_role')->where('role_id', $id)->pluck('permission_id')->toArray();
        $permissionGroups = GroupPermission::select('*')->with('permissions')->get();

        return view('admin.role.edit', compact('role','listPermission', 'permissionGroups'));
    }

        public function update(Request $request, string $id): RedirectResponse
    {
        \DB::beginTransaction(); // Bắt đầu giao dịch database

        try {
            // Gọi phương thức updateRole từ service và truyền dữ liệu request và id của vai trò
            $this->roleService->updateRole($id, $request->all());

            // Nếu có quyền được chọn
            if (!empty($request->permissions)) {
                // Lấy danh sách các quyền đã tồn tại cho vai trò
                $existingPermissions = \DB::table('permission_role')
                    ->where('role_id', $id)
                    ->pluck('permission_id')
                    ->toArray();

                // Duyệt qua từng quyền được chọn từ request
                foreach ($request->permissions as $permission) {
                    // Nếu quyền không tồn tại trong danh sách quyền của vai trò, thêm vào
                    if (!in_array($permission, $existingPermissions)) {
                        \DB::table('permission_role')->insert(['permission_id' => $permission, 'role_id' => $id]);
                    }
                }
            }

            \DB::commit(); // Lưu các thay đổi vào database
            return redirect()->route('role.index')->with('success', 'Role updated successfully.'); // Chuyển hướng về trang danh sách vai trò
        } catch (\Exception $exception) {
            \DB::rollBack(); // Rollback lại giao dịch nếu có lỗi
            return redirect()->back()->with('error', 'Failed to update role: ' . $exception->getMessage()); // Quay lại trang trước với thông báo lỗi
        }
    }


    public function destroy(string $id): RedirectResponse
    {
        $this->roleService->deleteRole($id);
        return redirect()->route('role.index')->with('success', 'Role deleted successfully.');
    }
}
