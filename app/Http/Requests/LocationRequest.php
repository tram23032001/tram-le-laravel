<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class LocationRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true; 
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name'          => 'required|max:255',
            'notes'         => 'nullable|string',
            'department_id' => 'required|exists:departments,id',
        ];
    }

    /**
     * Get the error messages for the defined validation rules.
     *
     * @return array
     */
    public function messages()
    {
        return [
            'name.required'          => 'Vui lòng nhập tên địa điểm',
            'name.max'               => 'Tên địa điểm không được vượt quá :max ký tự',
            'notes.string'           => 'Ghi chú phải là một chuỗi',
            'department_id.required' => 'Vui lòng chọn một bộ phận',
            'department_id.exists'   => 'Bộ phận đã chọn không tồn tại',
        ];
    }
}
