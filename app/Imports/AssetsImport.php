<?php

namespace App\Imports;

use App\Models\Asset;
use Illuminate\Support\Facades\Hash;
use Maatwebsite\Excel\Concerns\ToModel;
use Maatwebsite\Excel\Concerns\WithHeadingRow;
use Illuminate\Support\Carbon;
class AssetsImport implements ToModel, WithHeadingRow
{
    /**
     * Import a single row from the Excel file.
     *
     * @param array $row
     * @return \Illuminate\Database\Eloquent\Model|null
     */
    public function model(array $row)
    {
        // Tạo một bản ghi mới cho tài sản từ dữ liệu trong hàng của tệp Excel
        return new Asset([
            'name' => $row['name'], // Tên tài sản
            'notes' => $row['notes'], // Ghi chú
            'created_at' => $row['created_at'], // Ngày tạo
            'updated_at' => $row['updated_at'], // Ngày cập nhật
            'condition_id' => $row['condition_id'], // ID của điều kiện
            'category_id' => $row['category_id'], // ID của danh mục
            'location_id' => $row['location_id'], // ID của vị trí
            'model_id' => $row['model_id'], // ID của mô hình
            'supplier_id' => $row['supplier_id'], // ID của nhà cung cấp
            'serial' => $row['serial'], // Số serial
            'warranty' => $row['warranty'], // Bảo hành
            'price' => $row['price'], // Giá
        ]);
    }
}
