<?php

namespace App\Imports;

use App\Models\Location;
use Illuminate\Support\Facades\Hash;
use Maatwebsite\Excel\Concerns\ToModel;
use Maatwebsite\Excel\Concerns\WithHeadingRow;

class LocationsImport implements ToModel, WithHeadingRow
{
    /**
     * Import a single row from the Excel file.
     *
     * @param array $row
     * @return \Illuminate\Database\Eloquent\Model|null
     */
    public function model(array $row)
    {
        //
        return new Location([
            'name' => $row['name'], 
            'notes' => $row['notes'],
            'department_id' => $row['department_id'], 
        ]);
    }
}
