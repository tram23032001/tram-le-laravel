<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Asset extends Model
{
    protected $fillable = [
        'name',
        'code',
        'notes',
        'condition_id',
        'category_id',
        'location_id',
        'model_id',
        'supplier_id',
        'serial',
        'date',
        'warranty',
        'price',
        'created_at',
        'updated_at',
        'qr_code'
    ];

    protected $table = 'assets';

    public function condition()
    {
        return $this->belongsTo(Condition::class);
    }

    public function category()
    {
        return $this->belongsTo(Category::class);
    }

  
    public function location()
    {
        return $this->belongsTo(Location::class);
    }

  
    public function model()
    {
        return $this->belongsTo(AssetModel::class);
    }


    public function supplier()
    {
        return $this->belongsTo(Supplier::class);
    }

    public function formatQRData(): array
    {
        return [
            'Code' => $this->id,
            'Name' => $this->name,
            'Condition' => $this->condition->name,
            'Category' => $this->category->name,
            'Location' => $this->location->name,
            'Model' => $this->model->name
        ];
    }
}
