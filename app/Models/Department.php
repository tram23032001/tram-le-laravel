<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use App\Casts\Json;

class Department extends Model
{
    use HasFactory;
    protected $fillable = ['name', 'floor', 'unit', 'building', 'address', 'zip_code'];
    protected $casts = [
        'address' => Json::class,
    ];

     public function getAddress()
    {
        return $this->address;
    }
}
