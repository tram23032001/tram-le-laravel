<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsToMany;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Shanmuga\LaravelEntrust\Models\EntrustRole;
use Illuminate\Support\Facades\Config;
use App\Models\User;
class Role extends EntrustRole
{
    protected $fillable = ['name', 'description'];

    public function permissionRole()
    {
        return $this->belongsToMany(Permission::class, 'permission_role', 'role_id', 'permission_id');
    }
    public function users()
    {
        return $this->belongsToMany(Config::get('auth.providers.users.model'), Config::get('entrust.tables.role_user'), Config::get('entrust.foreign_keys.role'), Config::get('entrust.foreign_keys.user'));

    }
}
