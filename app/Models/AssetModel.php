<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class AssetModel extends Model
{
    use HasFactory;

    protected $table = 'models';

    protected $fillable = [
        'name',
    ];

    public $timestamps = true;

    public function manufacturer()
    {
        return $this->belongsTo(Manufacturer::class);
    }

}
