<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;

class Image extends Model
{
    use HasFactory;

    protected $fillable = ['name', 'url', 'asset_id'];

    public function asset(): BelongsTo
    {
        return $this->belongsTo(Asset::class);
    }
}
