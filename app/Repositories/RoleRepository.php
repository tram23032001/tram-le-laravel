<?php

namespace App\Repositories;

use App\Models\Role;
use App\Repositories\Eloquent\EloquentBaseRepository;

class RoleRepository extends EloquentBaseRepository
{
    /**
     * RoleRepository constructor.
     *
     * @param Role $role
     */
    public function __construct(Role $role)
    {
        parent::__construct($role);
    }

    /**
     * Get all roles with pagination.
     *
     * @param int $perPage
     * @return \Illuminate\Contracts\Pagination\LengthAwarePaginator
     */
    public function getAllRoles($perPage)
    {
        return Role::paginate($perPage);
    }
}
