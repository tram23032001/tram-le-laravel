<?php

namespace App\Repositories;

use App\Models\Image;
use App\Repositories\Eloquent\EloquentBaseRepository;

class ImageRepository extends EloquentBaseRepository
{
    /**
     * ImageRepository constructor.
     *
     * @param Image $image
     */
    public function __construct(Image $image)
    {
        parent::__construct($image);
    }

    /**
     * Create a new image.
     *
     * @param array $data
     * @return Image
     */

    /**
     * Get all images.
     *
     * @return \Illuminate\Database\Eloquent\Collection
     */

}
