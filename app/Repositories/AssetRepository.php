<?php

namespace App\Repositories;

use App\Models\Asset;
use App\Repositories\Eloquent\EloquentBaseRepository;

class AssetRepository extends EloquentBaseRepository
{
    /**
     * AssetRepository constructor.
     *
     * @param Asset $asset
     */
    public function __construct(Asset $asset)
    {
        parent::__construct($asset);
    }

    /**
     * Lấy tất cả các tài sản và phân trang chúng.
     *
     * @param int $perPage Số lượng bản ghi trên mỗi trang
     * @return \Illuminate\Contracts\Pagination\LengthAwarePaginator
     */
    public function getAllAssets($perPage)
    {
        return Asset::paginate($perPage);
    }
}
