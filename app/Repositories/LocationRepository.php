<?php

namespace App\Repositories;

use App\Models\Location;
use App\Repositories\Eloquent\EloquentBaseRepository;

class LocationRepository extends EloquentBaseRepository
{
    /**
     * LocationRepository constructor.
     *
     * @param Location $location
     */
    public function __construct(Location $location)
    {
        parent::__construct($location);
    }

    /**
     * Get all locations with pagination.
     *
     * @param int $perPage
     * @return \Illuminate\Contracts\Pagination\LengthAwarePaginator
     */
    public function getAllLocations($perPage)
    {
        return Location::paginate($perPage);
    }

}
