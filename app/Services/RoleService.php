<?php

namespace App\Services;

use App\Repositories\RoleRepository;
use Illuminate\Support\Facades\DB;
class RoleService
{
    protected $roleRepository;

    public function __construct(RoleRepository $roleRepository)
    {
        $this->roleRepository = $roleRepository;
    }

    /**
     * Get all roles with pagination.
     *
     * @param int $perPage
     * @return \Illuminate\Contracts\Pagination\LengthAwarePaginator
     */
    public function getAllRoles($perPage)
    {
        return $this->roleRepository->getAllRoles($perPage);
    }

    /**
     * Get a role by its ID.
     *
     * @param int $id
     * @return \App\Models\Role|null
     */
    public function getRoleById($id)
    {
        return $this->roleRepository->find($id);
    }

    /**
     * Create a new role.
     *
     * @param array $data
     * @return \App\Models\Role
     */
    public function createRole(array $data)
    {
        DB::beginTransaction();
        try {
            $role = $this->roleRepository->create($data);
            if (!empty($data['permissions'])) {
                foreach ($data['permissions'] as $permission) {
                    \DB::table('permission_role')->insert(['permission_id' => $permission, 'role_id' => $role->id]);
                }
            }
            DB::commit();
            return $role;
        } catch (\Exception $exception) {
            DB::rollBack();
            throw $exception;
        }
    }
    /**
     * Update a role.
     *
     * @param int $id
     * @param array $data
     * @return \App\Models\Role|null
     */
    public function updateRole($id, array $data)
    {
        $role = $this->roleRepository->find($id);
        if ($role) {
            $this->roleRepository->update($role, $data);
        }
        return $role;
    }

    /**
     * Delete a role.
     *
     * @param int $id
     * @return bool
     */
    public function deleteRole($id)
    {
        $role = $this->roleRepository->find($id);
        if ($role) {
            return $this->roleRepository->destroy($role);
        }
        return false;
    }
}
