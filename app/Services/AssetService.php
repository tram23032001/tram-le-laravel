<?php

namespace App\Services;

use App\Repositories\AssetRepository;
use Illuminate\Support\Str;
class AssetService
{
    protected $assetRepository;

    public function __construct(AssetRepository $assetRepository)
    {
        $this->assetRepository = $assetRepository;
    }

    public function getAllAssets($perPage)
    {
        return $this->assetRepository->getAllAssets($perPage);
    }

    public function getAssetById($id)
    {
        return $this->assetRepository->find($id);
    }

    public function createAsset(array $data)
    {
        // Tạo mã code ngẫu nhiên với độ dài 8 ký tự
        $data['code'] = Str::random(8);

        return $this->assetRepository->create($data);
    }

    public function updateAsset($id, array $data)
    {
        $asset = $this->assetRepository->find($id);
        if ($asset) {
            $this->assetRepository->update($asset, $data);
        }
        return $asset;
    }

    public function deleteAsset($id)
    {
        $asset = $this->assetRepository->find($id);
        if ($asset) {
            return $this->assetRepository->destroy($asset);
        }
        return false;
    }
}
