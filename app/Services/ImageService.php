<?php

namespace App\Services;

use App\Repositories\ImageRepository;

class ImageService
{
    protected $imageRepository;

    public function __construct(ImageRepository $imageRepository)
    {
        $this->imageRepository = $imageRepository;
    }

    public function createImage(array $data)
    {
        return $this->imageRepository->createImage($data);
    }

    public function getAllImages()
    {
        return $this->imageRepository->getAllImages();
    }

    // Bạn có thể thêm các phương thức khác tại đây nếu cần
}
