<?php

namespace App\Services;

use App\Repositories\LocationRepository;

class LocationService
{
    protected $locationRepository;

    public function __construct(LocationRepository $locationRepository)
    {
        $this->locationRepository = $locationRepository;
    }

    public function getAllLocations($perPage)
    {
        return $this->locationRepository->paginate($perPage);
    }

    public function getLocationById($id)
    {
        return $this->locationRepository->find($id);
    }

    public function createLocation(array $data)
    {
        return $this->locationRepository->create($data);
    }

    public function updateLocation($id, array $data)
    {
        $location = $this->locationRepository->find($id);
        if ($location) {
            $this->locationRepository->update($location, $data);
        }
        return $location;
    }

    public function deleteLocation($id)
    {
        $location = $this->locationRepository->find($id);
        if ($location) {
            return $this->locationRepository->destroy($location);
        }
        return false;
    }

 
}
