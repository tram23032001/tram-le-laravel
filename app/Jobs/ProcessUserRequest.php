<?php

namespace App\Jobs;

use App\Models\User;
use App\Mail\PasswordResetConfirmation;
use App\Mail\RegistrationVerification;
use Illuminate\Support\Facades\Mail;

class ProcessUserRequest implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    protected $user;
    protected $action;

    /**
     * Create a new job instance.
     *
     * @param User $user
     * @param string $action
     * @return void
     */
    public function __construct(User $user, $action)
    {
        $this->user = $user;
        $this->action = $action;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        if ($this->action === 'password_reset_confirmation') {
            // Logic to send password reset confirmation email
            Mail::to($this->user->email)->send(new PasswordResetConfirmation($this->user));
        } elseif ($this->action === 'registration_verification') {
            // Logic to send registration verification email
            Mail::to($this->user->email)->send(new RegistrationVerification($this->user));
        }
        // Add more logic for other actions if needed
    }
}
