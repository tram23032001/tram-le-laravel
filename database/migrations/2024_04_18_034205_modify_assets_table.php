<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::table('assets', function (Blueprint $table) {
           // Xóa cột condition cũ
           $table->dropColumn('condition');
            // Thêm cột condition_id mới
            $table->unsignedBigInteger('condition_id')->nullable();
            // Thêm khóa ngoại để nối với bảng conditions
            $table->foreign('condition_id')->references('id')->on('conditions')->onDelete('SET NULL');
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::table('assets', function (Blueprint $table) {
            $table->string('condition')->nullable();
            $table->dropForeign(['condition_id']);
            $table->dropColumn('condition_id');
        });
    }
};
