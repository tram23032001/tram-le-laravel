<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::table('assets', function (Blueprint $table) {
            // Thêm cột serial
            $table->string('serial')->nullable();
            // Thêm cột date
            $table->date('date')->nullable();
            // Thêm cột warranty
            $table->string('warranty')->nullable();
            // Thêm cột price
            $table->decimal('price', 10, 2)->nullable();
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::table('assets', function (Blueprint $table) {
            $table->dropColumn('serial');
            $table->dropColumn('date');
            $table->dropColumn('warranty');
            $table->dropColumn('price');
        });
    }
};
