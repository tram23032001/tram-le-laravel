<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    public function up(): void
    {
        Schema::table('users', function (Blueprint $table) {
            // Loại bỏ ràng buộc khóa ngoại
            $table->dropForeign(['role_id']);
            // Xóa cột role_id
            $table->dropColumn('role_id');
        });
    }

    public function down(): void
    {
        Schema::table('users', function (Blueprint $table) {
            // Thêm lại cột role_id
            $table->unsignedBigInteger('role_id')->nullable();
            // Thêm lại ràng buộc khóa ngoại (nếu cần)
            $table->foreign('role_id')->references('id')->on('roles')->onDelete('cascade');
        });
    }
};
