<?php
namespace Database\Factories;

use App\Models\Department;
use Illuminate\Database\Eloquent\Factories\Factory;
use Faker\Factory as FakerFactory;

class DepartmentFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = Department::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        $faker = FakerFactory::create(); // Tạo một instance của Faker

        return [
            'name' => $faker->unique()->word(),
            'floor' => $faker->numberBetween(1, 10),
            'unit' => $faker->word(),
            'building' => $faker->word(),
            'address' => [
                'street' => $faker->streetAddress,
                'city' => $faker->city,
                'state' => $faker->stateAbbr,
                'country' => $faker->country 
            ],
            'zip_code' => $faker->postcode(),
        ];
    }
}
