<?php

namespace Database\Factories;

use App\Models\AssetModel;
use Illuminate\Database\Eloquent\Factories\Factory;

/**
 * @template TModel of \App\Models\Model
 *
 * @extends \Illuminate\Database\Eloquent\Factories\Factory<TModel>
 */
class AssetModelFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var class-string<TModel>
     */
    protected $model = AssetModel::class;

    /**
     * Define the model's default state.
     *
     * @return array<string, mixed>
     */
    public function definition(): array
    {
        return [
            'name' => $this->faker->word,
            'manufacturer_id' => $this->faker->numberBetween(1, 20), 
        ];
    }
}
