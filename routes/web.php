<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\Auth\LoginController;
use App\Http\Controllers\Auth\RegisterController;
use App\Http\Controllers\Auth\VerificationController;
use App\Http\Controllers\UserController;
use App\Http\Controllers\ProvisionServer;
use App\Http\Controllers\UserPostController;



Route::get('/login', [LoginController::class, 'showLoginForm'])->name('admin.login');
Route::post('/login', [LoginController::class, 'login']);
require __DIR__.'/admin/auth.php';
Route::get('/logout', [LoginController::class, 'logout'])->name('logout');

Route::middleware(['auth'])->group(function () {
    require __DIR__.'/admin/admin.php';
    require __DIR__.'/admin/model.php';
    require __DIR__.'/location/location.php';
    require __DIR__.'/location/department.php';
    require __DIR__.'/role/role.php';
    require __DIR__.'/asset/asset.php';
});
