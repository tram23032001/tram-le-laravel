<?php

use App\Http\Controllers\AssetController;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\View;

Route::resource('assets', AssetController::class)->except(['show']);
Route::post('/assets/import', [AssetController::class, 'import'])->name('assets.import');
Route::get('/assets/export', [AssetController::class, 'export'])->name('assets.export');
Route::get('/assets/qrcodes', [AssetController::class, 'qrcode'])->name('assets.qrcode');
Route::get('/assets/select', [AssetController::class, 'selectAssets'])->name('assets.select');
Route::get('/assets/template', [AssetController::class, 'printTemplate'])->name('print.template');
Route::post('print-assets', [AssetController::class, 'printAssets'])->name('print.assets');
Route::get('print-result', [AssetController::class, 'showPrintResult'])->name('print.result');
