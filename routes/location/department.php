<?php

use App\Http\Controllers\LocationController;
use App\Http\Controllers\DepartmentController;
use Illuminate\Support\Facades\Route;

Route::get('/departments/{id}',  [DepartmentController::class, 'show']);
