<?php

use App\Http\Controllers\LocationController;
use App\Http\Controllers\DepartmentController;
use Illuminate\Support\Facades\Route;

Route::resource('locations', LocationController::class)->except(['show'])->middleware('permission:Add-location');
Route::get('/departments/{id}',  [DepartmentController::class, 'show']);
Route::post('/locations/import', [LocationController::class, 'import'])->name('locations.import');