<?php
use App\Http\Controllers\RoleController;

Route::resource('role', RoleController::class);
Route::post('role/create', [RoleController::class, 'store']);
Route::post('role/{role}/edit', [RoleController::class, 'update']);