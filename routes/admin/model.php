<?php
use App\Models\AssetModel;
use App\Http\Controllers\ModelController;
use Illuminate\Support\Facades\Route;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\View;

Route::get('/models/by-manufacturer/{manufacturerId}', [ModelController::class, 'getModelsByManufacturer']);
