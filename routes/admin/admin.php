<?php
use App\Models\User;
use App\Http\Controllers\AdminController;
use App\Http\Controllers\HomeController;
use Illuminate\Support\Facades\Route;
use Illuminate\Support\Facades\Cookie;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use App\View\Composers\UserComposer;
use Illuminate\Support\Facades\View;

Route::resource('admins', AdminController::class)->except(['show']);
Route::get('admin/dashboard', [HomeController::class, 'index'])->name('admin.home');